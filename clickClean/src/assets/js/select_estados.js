$(function estados(){
    // Cargamos los estados
    var estados = "<option value='' disabled selected>Selecciona el estado</option>";

    for (var key in municipios) {
        if (municipios.hasOwnProperty(key)) {
            estados = estados + "<option value='" + key + "' id='"+key+"'>" + key + "</option>";
        }
    }
    $("#estado").each(function () {
        $(this).html(estados);
    });
    $("#estado2").each(function () {
        $(this).html(estados);
    });
    // Al detectar
    $( "#estado" ).change(function() {
        var html = "<option value='' disabled selected>Selecciona el municipio</option>";
        $( "#estado option:selected" ).each(function() {
            var estado = $(this).text();
            if(estado != "Selecciona el estado"){
                var municipio = municipios[estado];
                for (var i = 0; i < municipio.length; i++)
                    html += "<option value='" + municipio[i] + "'id='"+municipio[i]+"'>" + municipio[i] + "</option>";
            }
        });
        $('#municipio').html(html);
    })
    .trigger( "change" );

    // Al detectar
    $( "#estado2" ).change(function() {
        var html = "<option value='' disabled selected>Selecciona el municipio</option>";
        $( "#estado2 option:selected" ).each(function() {
            var estado = $(this).text();
            if(estado != "Selecciona el estado"){
                var municipio = municipios[estado];
                for (var i = 0; i < municipio.length; i++)
                    html += "<option value='" + municipio[i] + "'>" + municipio[i] + "</option>";
            }
        });
        $('#municipio2').html(html);
    })
    .trigger( "change" );
});