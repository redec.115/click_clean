import { Injectable } from '@angular/core';
import { HttpClient, HttpParams  } from '@angular/common/http';
import { JwtResponse } from '../models/jwt-response';
import { tap } from 'rxjs/operators';
import { Observable, BehaviorSubject, from } from 'rxjs';
import { EmailValidator } from '@angular/forms';
import { GlobalConstants} from '../models/constantes';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  AUTH_SERVER: string = GlobalConstants.nodeURL;
  authSubject = new BehaviorSubject(false);
  private token: string;
  constructor(private httpClient: HttpClient) { 
   
  }
  

  register(user: any,esEmpleado:boolean): Observable<JwtResponse> {
    console.log('registrando');
    console.log(user);
    return this.httpClient.post<JwtResponse>(`${this.AUTH_SERVER}/auth/register`,
      user).pipe(tap(
        (res: JwtResponse) => {
          console.log(res);
          if (res.dataUser!=null && !esEmpleado) {
            // guardar token
            console.log(res.dataUser);
            this.saveToken(
              res.dataUser.id_usuario.toString(),
              res.dataUser.accessToken, 
              res.dataUser.expiresIn,
              res.dataUser.email_usuario,
              res.dataUser.nombre_usuario,
              res.dataUser.celular_usuario,
              res.dataUser.nombre_completo);
            return res;
          }else{
            
            
            return false;
          }
        })
      );
  }

  login(user: any): Observable<JwtResponse> {
    console.log(user);
    return this.httpClient.post<JwtResponse>(`${this.AUTH_SERVER}/auth/login`,
      user).pipe(tap(
        (res: JwtResponse) => {
          console.log(res.dataUser);
          if (res.dataUser!=null) {
            // guardar token
            
            this.saveToken(
              res.dataUser.id_usuario.toString(),
              res.dataUser.accessToken, 
              res.dataUser.expiresIn,
              res.dataUser.email_usuario,
              res.dataUser.nombre_usuario,
              res.dataUser.celular_usuario,
              res.dataUser.nombre_completo);
            return res;
          }else{
            console.log('Contraseña o correo incorrectos');

            return false;
          }
        })
      );
  }

  getTipoUsuario():Observable<JwtResponse> {
    
    var email=this.getEmail();
    return this.httpClient.get<JwtResponse>(`${this.AUTH_SERVER}/auth/tipo`, 
    {params:new HttpParams().set('email', email),
      headers:{email:email,token:this.getToken()}});
   }
  logout(): void {
    this.token = '';
    localStorage.clear();
  }

  private saveToken(id:string,token: string, expiresIn: string, email: string, nombre: string, telefono: string,nombre_completo:string): void {
    localStorage.setItem("ID", id);
    localStorage.setItem("ACCESS_TOKEN", token);
    localStorage.setItem("EXPIRES_IN", expiresIn);
    localStorage.setItem("EMAIL",email);
    localStorage.setItem("NOMBRE",nombre);
    localStorage.setItem("TELEFONO",telefono);
    localStorage.setItem("NOMBRE_COMPLETO",nombre_completo);
    this.token = token;
  }

  
  getID(): string{
    var id="";
    if(this.token){
      return localStorage.getItem("ID");
    }
    return id;
  }
   getEmail(): string{
    var email="";
    if(this.token){
      return localStorage.getItem("EMAIL");
    }
    return email;
  }
  getNombre(): string{
    var nombre="";
    if(this.token){
      return localStorage.getItem("NOMBRE");
    }
    return nombre;
  }
  getTelefono(): string{
    var tel="";
    if(this.token){
      return localStorage.getItem("TELEFONO");
    }
    return tel;
  }
  getToken(): string {
    if (!this.token) {
      this.token = localStorage.getItem("ACCESS_TOKEN");
    }
    return this.token;
  }
  guardarTelefono(telefono):void{
    localStorage.setItem("TELEFONO",telefono);
  }
  //direccion temporal
  guardarDireccion(lat_lng):void{
    localStorage.setItem("LATITUD",lat_lng.latitud);
    localStorage.setItem("LONGITUD",lat_lng.longitud);
  }
  getLatitud():string{
    return localStorage.getItem("LATITUD");
  }
  getLongitud():string{
    return localStorage.getItem("LONGITUD");
  }
  //tiempos temporales
  guardarDiasDeLaSemana(dia_inicio,dia_fin):void{
    localStorage.setItem("DIA_INICIO",dia_inicio);
    localStorage.setItem("DIA_FIN",dia_fin);
  }
  getDia_Inicio():string{
    return localStorage.getItem("DIA_INICIO");
  }
  getDia_Final():string{
    return localStorage.getItem("DIA_FIN");
  }
  guardarHoras(hora_inicio,hora_fin):void{
    localStorage.setItem("HORA_INICIO",hora_inicio);
    localStorage.setItem("HORA_FIN",hora_fin);
  }
  getHora_inicio():string{
    return localStorage.getItem("HORA_INICIO");
  }
  getHora_fin():string{
    return localStorage.getItem("HORA_FIN");
  }

  guardarFechas(fecha_inicio,fecha_fin):void{
    localStorage.setItem("FECHA_INICIO",fecha_inicio);
    localStorage.setItem("FECHA_FIN",fecha_fin);
  }
  getFecha_inicio():string{
    return localStorage.getItem("FECHA_INICIO");
  }
  getFecha_fin():string{
    return localStorage.getItem("FECHA_FIN");
  }
  guardarId_Direccion(id):void{
    localStorage.setItem("ID_DIRECCION",id);
  }
  getId_Direccion():string{
    return localStorage.getItem("ID_DIRECCION");
  }

  guardarTokenStripe(token):void{
    localStorage.setItem("TOKEN_STRIPE",token);
  }
  getTokenStripe():any{
    return localStorage.getItem("TOKEN_STRIPE");
  }
  deleteTokenStripe():void{
    localStorage.removeItem("TOKEN_STRIPE");
  }
}