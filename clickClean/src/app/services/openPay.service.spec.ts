/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { OpenPayService } from './openPay.service';

describe('Service: OpenPay', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OpenPayService]
    });
  });

  it('should ...', inject([OpenPayService], (service: OpenPayService) => {
    expect(service).toBeTruthy();
  }));
});
