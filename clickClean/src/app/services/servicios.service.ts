import { Injectable } from '@angular/core';
import { HttpClient, HttpParams  } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Observable, BehaviorSubject, from } from 'rxjs';
import { GlobalConstants} from '../models/constantes';
import { AuthService} from './auth.service';
@Injectable({
  providedIn: 'root'
})
export class ServiciosService {

  AUTH_SERVER: string = GlobalConstants.nodeURL;
  constructor(private http: HttpClient,private authService:AuthService) { }

  obtenerServicios():Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/servicios`,
    {headers:{token:this.authService.getToken()}});
  }
  obtenerProductos():Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/servicios/productos`,
    {headers:{token:this.authService.getToken()}});
  }
  crearServicio(servicio):Observable<any>{
    return this.http.post<any>(`${this.AUTH_SERVER}/servicios`,servicio,
    {headers:{token:this.authService.getToken()}});
  }
  agregarProducto(producto):Observable<any>{
    return this.http.post<any>(`${this.AUTH_SERVER}/servicios/productos`,producto,
    {headers:{token:this.authService.getToken()}});
  }
  agregarHabilidadServicio(habilidad):Observable<any>{
    return this.http.post<any>(`${this.AUTH_SERVER}/servicios/habilidad`,habilidad,
    {headers:{token:this.authService.getToken()}});
  }
  eliminarHabilidadServicio(habilidad):Observable<any>{
    return this.http.post<any>(`${this.AUTH_SERVER}/servicios/eliminarhabilidad`,habilidad,
    {headers:{token:this.authService.getToken()}});
  }
  agregarImagenServicio(img):Observable<any>{
    return this.http.post<any>(`${this.AUTH_SERVER}/servicios/imagen`,img,
    {headers:{token:this.authService.getToken()}});
  }

  habilidadesDeServicio(id):Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/servicios/habilidades/${id}`,
    {headers:{token:this.authService.getToken()}});
  }

  deshabilitarServicio(id):Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/servicios/deshabilitar/${id}`,
    {headers:{token:this.authService.getToken()}});
  }
  editarServicio(data):Observable<any>{
    return this.http.post<any>(`${this.AUTH_SERVER}/servicios/editar`,data,
    {headers:{token:this.authService.getToken()}});
  }
}
