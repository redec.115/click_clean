/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ContratosService } from './contratos.service';

describe('Service: Contratos', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ContratosService]
    });
  });

  it('should ...', inject([ContratosService], (service: ContratosService) => {
    expect(service).toBeTruthy();
  }));
});
