/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { HabilidadesService } from './habilidades.service';

describe('Service: Habilidades', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HabilidadesService]
    });
  });

  it('should ...', inject([HabilidadesService], (service: HabilidadesService) => {
    expect(service).toBeTruthy();
  }));
});
