import { Injectable } from '@angular/core';
import { HttpClient, HttpParams  } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Observable, BehaviorSubject, from } from 'rxjs';
import { GlobalConstants} from '../models/constantes';
import { AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class HabilidadesService {

  AUTH_SERVER: string = GlobalConstants.nodeURL;
  constructor(private http: HttpClient,private authService:AuthService) { }

  obtenerHabilidades():Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/habilidades`,
    {headers:{token:this.authService.getToken()}});
  }
  crearHabilidad(habilidad):Observable<any>{
    return this.http.post<any>(`${this.AUTH_SERVER}/habilidades`,habilidad,
    {headers:{token:this.authService.getToken()}});
  }
  editarHabilidad(habilidad):Observable<any>{
    return this.http.post<any>(`${this.AUTH_SERVER}/habilidades/editar`,habilidad,
    {headers:{token:this.authService.getToken()}});
  }
  habilidadDeEmpleado(id):Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/habilidades/empleado/${id}`,
    {headers:{token:this.authService.getToken()}});
  }

  agregarHabilidadEmpleado(habilidad):Observable<any>{
    return this.http.post<any>(`${this.AUTH_SERVER}/habilidades/empleado/habilidad`,habilidad,
    {headers:{token:this.authService.getToken()}});
  }
  eliminarHabilidadEmpleado(habilidad):Observable<any>{
    return this.http.post<any>(`${this.AUTH_SERVER}/habilidades/empleado/eliminar`,habilidad,
    {headers:{token:this.authService.getToken()}});
  }

}
