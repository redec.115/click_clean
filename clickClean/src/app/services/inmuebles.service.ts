import { Injectable } from '@angular/core';
import { HttpClient, HttpParams  } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Observable, BehaviorSubject, from } from 'rxjs';
import { GlobalConstants} from '../models/constantes';
import { AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class InmueblesService {

  AUTH_SERVER: string = GlobalConstants.nodeURL;
  constructor(private http: HttpClient,private authService:AuthService) { }

  obtenerInmuebles():Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/inmuebles`,
    {headers:{token:this.authService.getToken()}});
  }
  inmueblesDeCliente(id):Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/inmuebles/cliente/${id}`,
    {headers:{token:this.authService.getToken()}});
  }

  crearInmueble(data):Observable<any>{
    return this.http.post<any>(`${this.AUTH_SERVER}/inmuebles`,data,
    {headers:{token:this.authService.getToken()}});
  }
}
