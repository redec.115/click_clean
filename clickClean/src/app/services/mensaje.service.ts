import { Injectable } from '@angular/core';
import { HttpClient, HttpParams  } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Observable, BehaviorSubject, from } from 'rxjs';
import { GlobalConstants} from '../models/constantes';
import { AuthService} from './auth.service';
@Injectable({
  providedIn: 'root'
})
export class MensajeService {

  AUTH_SERVER: string = GlobalConstants.nodeURL;
  constructor(private http: HttpClient,private authService:AuthService) { }

  enviarMensaje(mensaje):Observable<any>{
    return this.http.post<any>(`${this.AUTH_SERVER}/mensaje/enviarmensaje`,mensaje,
    {headers:{token:this.authService.getToken()}});
  }
  //https://api.whatsapp.com/send?phone=nuestronumero&text=nuestromensaje
  
}
