import { Injectable } from '@angular/core';
import { HttpClient, HttpParams  } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Observable, BehaviorSubject, from } from 'rxjs';
import { GlobalConstants} from '../models/constantes';
import { AuthService} from './auth.service';
@Injectable({
  providedIn: 'root'
})
export class ImagenesService {

  AUTH_SERVER: string = GlobalConstants.nodeURL;
  constructor(private http: HttpClient,private authService:AuthService) { }

  guardarImagen(img:FormData){
    return this.http.post<any>(`${this.AUTH_SERVER}/imagenes`,img,
    {headers:{token:this.authService.getToken()}});
  }

  getImagen(path):Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/imagenes/${path}`,
    {headers:{token:this.authService.getToken()}});
  }

  eliminarImagen(img):Observable<any>{
    return this.http.delete<any>(`${this.AUTH_SERVER}/imagenes/${img}`,
    {headers:{token:this.authService.getToken()}});
  }
}
