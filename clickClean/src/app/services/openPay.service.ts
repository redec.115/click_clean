import { Injectable } from '@angular/core';
import { HttpClient, HttpParams  } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Observable, BehaviorSubject, from } from 'rxjs';
import { GlobalConstants} from '../models/constantes';
import { AuthService} from './auth.service';
import { CardOP } from '../models/cardsOP';
import { chargeOP } from '../models/chargeOP';
import { CustomerOP } from '../models/customerOP';
@Injectable({
  providedIn: 'root'
})
export class OpenPayService {

  AUTH_SERVER: string = GlobalConstants.nodeURL;
  constructor(private http: HttpClient,private authService:AuthService) { }

  crearCliente(cliente:CustomerOP):Observable<any>{
    return this.http.post<any>(`${this.AUTH_SERVER}/openpay/customer`,cliente,
    {headers:{token:this.authService.getToken()}});
  }
  getOpenPayIDCustomer(id):Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/openpay/customer/${id}`,
    {headers:{token:this.authService.getToken()}});
  }

  eliminarCliente(id:any):Observable<any>{
    return this.http.delete<any>(`${this.AUTH_SERVER}/openpay/customer/${id}`,
    {headers:{token:this.authService.getToken()}});
  }

  getTarjetasLista():Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/openpay/card`,
    {headers:{token:this.authService.getToken()}});
  }

  crearTarjeta(tarjeta:CardOP):Observable<any>{
    return this.http.post<any>(`${this.AUTH_SERVER}/openpay/card/customer`,tarjeta,
    {headers:{token:this.authService.getToken()}});
  }

  getTarjetasCliente(id:any):Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/openpay/card/customer/${id}`,
    {headers:{token:this.authService.getToken()}});
  }

  eliminarTarjeta(data:any):Observable<any>{
    return this.http.post<any>(`${this.AUTH_SERVER}/openpay/card/customer/delete`,data,
    {headers:{token:this.authService.getToken()}});
  }
  crearCargo(cargo:chargeOP):Observable<any>{
    return this.http.post<any>(`${this.AUTH_SERVER}/openpay/charge`,cargo,
    {headers:{token:this.authService.getToken()}});
  }

  totalConComision(total:number) {
    return total+
    ((total*GlobalConstants.comisionPorcentajeOpenPay+GlobalConstants.comisionFijaOpenPay)
    *GlobalConstants.iva)
  }
  initOpenPay(){
    OpenPay.setId(GlobalConstants.OpenPayID);
    OpenPay.setApiKey(GlobalConstants.OpenPayKey);
    OpenPay.setSandboxMode(true);
    return OpenPay.deviceData.setup("payment-form", "deviceIdHiddenFieldName");
  }
}
