import { Injectable } from '@angular/core';
import { HttpClient, HttpParams  } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Observable, BehaviorSubject, from } from 'rxjs';
import { GlobalConstants} from '../models/constantes';
import { AuthService} from './auth.service';
@Injectable({
  providedIn: 'root'
})
export class ContratosService {

  AUTH_SERVER: string = GlobalConstants.nodeURL;
  constructor(private http: HttpClient,private authService:AuthService) { }

  obtenerMisContratos(id):Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/contratos/miscontratos/${id}`,
    {headers:{token:this.authService.getToken()}});
  }
  obtenerMisCompras(id):Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/contratos/miscompras/${id}`,
    {headers:{token:this.authService.getToken()}});
  }
  detalleContrato(id):Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/contratos/detalle/${id}`,
    {headers:{token:this.authService.getToken()}});
  }

  detalleCompra(id):Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/contratos/detallecompra/${id}`,
    {headers:{token:this.authService.getToken()}});
  }
  agregarServicioCarrito(data):Observable<any>{
    return this.http.post<any>(`${this.AUTH_SERVER}/contratos/carrito/servicio`,data,
    {headers:{token:this.authService.getToken()}});
  }
  agregarProductoCarrito(data):Observable<any>{
    return this.http.post<any>(`${this.AUTH_SERVER}/contratos/carrito/producto`,data,
    {headers:{token:this.authService.getToken()}});
  }
  eliminarServicioCarrito(data):Observable<any>{
    return this.http.post<any>(`${this.AUTH_SERVER}/contratos/carrito/servicio/eliminar`,data,
    {headers:{token:this.authService.getToken()}});
  }
  eliminarProductoCarrito(data):Observable<any>{
    return this.http.post<any>(`${this.AUTH_SERVER}/contratos/carrito/producto/eliminar`,data,
    {headers:{token:this.authService.getToken()}});
  }

  vaciarCarrito(id):Observable<any>{
    return this.http.delete<any>(`${this.AUTH_SERVER}/contratos/carrito/servicio/${id}`,
    {headers:{token:this.authService.getToken()}});
  }

  consultarMiCarrito(id):Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/contratos/carrito/${id}`,
    {headers:{token:this.authService.getToken()}});
  }

  getEmpleadosDisponiblesDeServicio(id):Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/contratos/servicios/empleados/${id}`,
    {headers:{token:this.authService.getToken()}});
  }

  agregarContrato(data):Observable<any>{
    return this.http.post<any>(`${this.AUTH_SERVER}/contratos/carrito/contratar`,data,
    {headers:{token:this.authService.getToken()}});
  }
  agregarServicioContrato(data):Observable<any>{
    return this.http.post<any>(`${this.AUTH_SERVER}/contratos/carrito/contrata/servicios`,data,
    {headers:{token:this.authService.getToken()}});
  }
  getContratosActivos():Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/contratos/estatus/activos`,
    {headers:{token:this.authService.getToken()}});
  }
  getContratosProximos():Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/contratos/estatus/proximos`,
    {headers:{token:this.authService.getToken()}});
  }
  getContratosHistorial():Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/contratos/estatus/historial`,
    {headers:{token:this.authService.getToken()}});
  }
}
