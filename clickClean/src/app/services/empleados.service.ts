import { Injectable } from '@angular/core';
import { HttpClient, HttpParams  } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Observable, BehaviorSubject, from } from 'rxjs';
import { GlobalConstants} from '../models/constantes';
import { AuthService} from './auth.service';
@Injectable({
  providedIn: 'root'
})
export class EmpleadosService {

  AUTH_SERVER: string = GlobalConstants.nodeURL;
  constructor(private http: HttpClient,private authService:AuthService) { }

  obtenerEmpleados():Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/empleados`,
    {headers:{token:this.authService.getToken()}});
  }

  getContratosDeEmpleados(id):Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/empleados/contratos/${id}`,
    {headers:{token:this.authService.getToken()}});
  }

}
