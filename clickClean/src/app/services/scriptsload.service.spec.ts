import { TestBed } from '@angular/core/testing';

import { ScriptsloadService } from './scriptsload.service';

describe('ScriptsloadService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ScriptsloadService = TestBed.get(ScriptsloadService);
    expect(service).toBeTruthy();
  });
});
