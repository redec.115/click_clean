import { Injectable } from '@angular/core';
interface Scripts {
  name: string;
  src: string;
  datapreferenceid: string;
}

export const ScriptStore: Scripts[] = [
  { name: 'demo1', src: '../../assets/js/demo1.js',datapreferenceid:''},
  {name: 'polyfills', src: '../../assets/js/polyfills.js' ,datapreferenceid:''},
  { name: 'modernizr-2.6.2', src: '../../assets/js/modernizr-2.6.2.min.js',datapreferenceid:''},
  {name: 'quantity', src: '../../assets/js/quantity.js', datapreferenceid:''},
  {name: 'input', src: '../../assets/js/input.js', datapreferenceid:''},
  {name: 'jquery-3.2.1', src: '../../assets/js/jquery/jquery-3.2.1.min.js', datapreferenceid:''},
  {name: 'jquery.waypoints', src: '../../assets/js/jquery/jquery.waypoints.min.js', datapreferenceid:''},
  {name: 'progressbar', src: '../../assets/js/progressbar.js', datapreferenceid:''},
  {name: 'estados_select', src: '../../assets/js/select_estados.js', datapreferenceid:''},
  {name: 'municipios', src: '../../assets/js/municipios.js', datapreferenceid:''},
  {name: 'textnote', src: '../../assets/js/textnote.js', datapreferenceid:''},
  {name: 'canvasjs', src: '../../assets/js/charts/canvasjs.min.js', datapreferenceid:''},
  {name: 'canvasjsreact', src: '../../assets/js/charts/canvasjs.react.js', datapreferenceid:''},
  {name: 'jquerycanvasjs', src: '../../assets/js/charts/jquery.canvasjs.min.js', datapreferenceid:''},

 
]
@Injectable({
  providedIn: 'root'
})
export class ScriptsloadService {
  private scripts: any = [];
  constructor() { 
    ScriptStore.forEach((script: any) => {
      this.scripts[script.name] = {
        loaded: false,
        src: script.src,
        datapreferenceid: script.datapreferenceid
      };
    });
  }

  load(...scripts: string[]) {
    const promises: any[] = [];
    scripts.forEach((script) => promises.push(this.loadScript(script)));
    return Promise.all(promises);
  }

  loadScript(name: string) {
    return new Promise((resolve, reject) => {
      if (!this.scripts[name].loaded) {
        //load script
        let script: any = document.createElement('script');
        script.type = 'text/javascript';
        script.src = this.scripts[name].src;
        script.datapreferenceid = this.scripts[name].datapreferenceid;
        if (script.readyState) {  //IE
            script.onreadystatechange = () => {
                if (script.readyState === "loaded" || script.readyState === "complete") {
                    script.onreadystatechange = null;
                    this.scripts[name].loaded = true;
                    resolve({script: name, loaded: true, status: 'Loaded'});
                }
            };
        } else {  //Others
            script.onload = () => {
                this.scripts[name].loaded = true;
                resolve({script: name, loaded: true, status: 'Loaded'});
            };
        }
        script.onerror = (error: any) => resolve({script: name, loaded: false, status: 'Loaded'});
        document.getElementsByTagName('head')[0].appendChild(script);
      } else {
        resolve({ script: name, loaded: true, status: 'Already Loaded' });
      }
    });
  }

}
