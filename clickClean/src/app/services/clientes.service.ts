import { Injectable } from '@angular/core';
import { HttpClient, HttpParams  } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Observable, BehaviorSubject, from } from 'rxjs';
import { GlobalConstants} from '../models/constantes';
import { AuthService} from './auth.service';
@Injectable({
  providedIn: 'root'
})
export class ClientesService {

  AUTH_SERVER: string = GlobalConstants.nodeURL;
  constructor(private http: HttpClient,private authService:AuthService) { 
   
  }
  obtenerClientes():Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/clientes`,
    {headers:{token:this.authService.getToken()}});
  }

  referenciasCliente(id):Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/clientes/referencias/${id}`,
    {headers:{token:this.authService.getToken()}});
  }

  getEstoyReferenciado(id):Observable<any>{
    return this.http.get<any>(`${this.AUTH_SERVER}/clientes/mireferencia/${id}`,
    {headers:{token:this.authService.getToken()}});
  }

  agregarReferenciaCliente(data):Observable<any>{
    return this.http.post<any>(`${this.AUTH_SERVER}/clientes/referencias`,data,
    {headers:{token:this.authService.getToken()}});
  }
  agregarImagenUsuario(data):Observable<any>{
    return this.http.post<any>(`${this.AUTH_SERVER}/clientes/imagen`,data,
    {headers:{token:this.authService.getToken()}});
  }

}
