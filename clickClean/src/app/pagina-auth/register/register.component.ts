import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../services/auth.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private authService:AuthService
  ) { }
  terminos: any = {
    status: false
  };
  myPassword = new FormControl('', [
    Validators.required,
    Validators.pattern('[a-zA-Z0-9]{8,}') // <-- Here's how you pass in the custom validator using regex.
  ]);
  myPassword2 = new FormControl('', [
    Validators.required,
    Validators.pattern('[a-zA-Z0-9]{8,}') // <-- Here's how you pass in the custom validator using regex.
  ]);
  myEmail = new FormControl('', [
    Validators.required,
    Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')
  ]);
  myCel = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9]{10}$')
  ]);
  myTel = new FormControl('', [
    Validators.required
  ]);
  myName = new FormControl('', [
    Validators.required,
    Validators.pattern('^[a-zA-Z ñÑ]*$')
  ]);
  mySecondName = new FormControl('', [
    Validators.required,
    Validators.pattern('^[a-zA-Z ñÑ]*$')
  ]);
  mySecondName2 = new FormControl('', [
    Validators.pattern('^[a-zA-Z ñÑ]*$')
  ]);
  myDate = new FormControl('', [
    Validators.required,
  ]);
  FormGroupRegister: FormGroup;
  ngOnInit() {
    //Formulario de datos personales
    this.FormGroupRegister = this.formBuilder.group({
      nombre: this.myName,
      ap_materno: this.mySecondName,
      ap_paterno: this.mySecondName2,
      telefono: this.myTel,
      celular: this.myCel,
      fecha_nac: this.myDate,
      email: this.myEmail,
      password: this.myPassword,
      password2: this.myPassword2,
    });
  }
  changeStatus(event) {
    this.terminos.status = event.checked;
  }
  onRegister(form) {
    console.log(form);
    if (form.status == 'VALID') {
      if(form.value.password==form.value.password2){
        if (this.terminos.status) {
          console.log(form.value);
          let fecha: Date = new Date(form.value.fecha_nac);
          let mes;
          let dia;
          if ((fecha.getMonth() + 1) < 10)
            mes = 0 + "" + (fecha.getMonth() + 1);
          else
            mes = (fecha.getMonth() + 1);
          if ((fecha.getDate()) < 10)
            dia = 0 + "" + (fecha.getDate());
          else
            dia = (fecha.getDate());
  
          console.log(fecha.getFullYear() + "-" + mes + "-" + dia);
          let data: any = {
            ap_materno_usuario: form.value.ap_materno,
            ap_paterno_usuario: form.value.ap_paterno,
            celular_usuario: form.value.celular,
            email_usuario: form.value.email,
            email:form.value.email,//este es para el login
            fecha_nacimiento_usuario: fecha.getFullYear() + "-" + mes + "-" + dia,
            nombre_usuario: form.value.nombre,
            telcasa_usuario: form.value.telefono,
            id_rol:3,
            password:form.value.password
          }
          console.log(data);
          this.authService.register(data,false).subscribe(
            res=>{
              console.log(res);
              this.authService.login(data).subscribe(
                res=>{
                  this.authService.getToken();
                  console.log(this.authService.getToken());
                  this.toastr.success(data.nombre_usuario,"Bienvenido");
                  this.router.navigateByUrl('/index');
                }
              )
            }
          )
        } else {
          this.toastr.warning("Acepte los terminos y condiciones", "Terminos y condiciones");
        }
      }else{
        this.toastr.warning("Las contraseñas no coinciden", "Contraseña");
      }
    } else {
      this.toastr.warning("Llene sus datos correctamente");
    }

  }
}
