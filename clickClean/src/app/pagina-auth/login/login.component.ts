import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../services/auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private toastr: ToastrService,
    private authService: AuthService,
    private router: Router,) { 
    
  }

  ngOnInit() {
    
  }
  toastPrueba(f){
    this.toastr.success("Prueba",);
  }
  onLogin(form): void {
    console.log(form);
    var datos: any;
    if (form.value != null)
      datos = form.value;
    else
      datos = form;

    console.log(datos);
    this.authService.login(datos).subscribe(res => {
      if (res.dataUser != null) {
        //this.router.navigateByUrl('/admin/home');

        this.toastr.success("Bienvenido " + res.dataUser.nombre_usuario);
        if (res.dataUser.rol == 'ADMINISTRADOR')
          this.router.navigateByUrl("/admin/principal");
        else if (res.dataUser.rol == 'CLIENTE')
          this.router.navigateByUrl('/index');
        else if (res.dataUser.rol == 'EMPLEADO')
          this.router.navigateByUrl('/socio/contratos');
          
      }//
      else {
        this.toastr.error("Correo o contraseña incorrectos");
        this.router.navigateByUrl('/auth/login');

      }

    },
      err => {
        console.log(err);
      });

  }
}
