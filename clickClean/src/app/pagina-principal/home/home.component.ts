import { Component, OnInit } from '@angular/core';
import { GlobalConstants } from 'src/app/models/constantes';
import { ServiciosService } from '../../services/servicios.service';
import { HabilidadesService } from '../../services/habilidades.service';
import { ContratosService } from '../../services/contratos.service';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private serviciosService: ServiciosService,
    private habilidadesService: HabilidadesService,
    private contratosService: ContratosService,
    private authService: AuthService,
    private toastr: ToastrService,
    private router: Router,
    private sanitizer: DomSanitizer) { 
      this.urlVideo=this.sanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/"+this.id+"?autoplay=1&rel=0&controls=0&showinfo=0&disablekb=1&start=0&playsinline=1&mute=1&loop=1&playlist="+this.id);
    }
    id="bNcKBPHvAX0";
  user: any = null;
  servidor: string = GlobalConstants.nodeURL;
  urlVideo:any;
  carrito = 0;
  total =0;
  servicios:any;
  ngOnInit() {
    this.verificarSesion();
  }
  private async verificarSesion() {
    console.log('Verificando', 'Tipo de usuario');
    var token = this.authService.getToken();

    if (this.authService.getToken() != null)
      this.authService.getTipoUsuario().subscribe(res => {
        let x: any = res;
        console.log(res);
        if (x == "EXPIRO") {
          this.toastr.info("Su sesion expiro");
          this.router.navigateByUrl('/auth/login');
        }
        if (token != null) {
          if (res[0].rol == "ADMINISTRADOR" || res[0].rol == "EMPLEADO" || res[0].rol == "CLIENTE" && token != null) {
            this.user = res[0];
            this.getCarrito();
            this.getServicios();
            //console.log(res.dataUser.accessToken);
          }
        }
      }, err => {
        console.log(err);
      });
  }

  getCarrito() {
    this.contratosService.consultarMiCarrito(this.user.id_usuario).subscribe(
      res => {
        console.log(res);
        this.total=0;
        this.carrito = res.length;
        res.forEach(element => {
          this.total+=parseFloat(element.costo_servicio);
        });
      }
    );
  }
  getServicios() {
    this.serviciosService.obtenerServicios().subscribe(
      res => {
        this.servicios = res;
        console.log(res);
      }
    );
  }

  logout() {
    this.authService.logout();
    this.user = null;
  }

}
