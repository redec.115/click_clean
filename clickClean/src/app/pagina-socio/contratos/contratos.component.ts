import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { GlobalConstants } from 'src/app/models/constantes';
import { ServiciosService } from '../../services/servicios.service';
import { HabilidadesService } from '../../services/habilidades.service';
import { InmueblesService } from '../../services/inmuebles.service';
import { ContratosService } from '../../services/contratos.service';
import { EmpleadosService } from '../../services/empleados.service';
import { AuthService } from '../../services/auth.service';
import { ImagenesService } from '../../services/imagenes.service';
import { ScriptsloadService } from '../../services/scriptsload.service';
import { OpenPayService } from '../../services/openPay.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { chargeOP } from 'src/app/models/chargeOP';
import { CustomerOP } from 'src/app/models/customerOP';
import { CardOP } from 'src/app/models/cardsOP';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatTableDataSource } from '@angular/material/table';
@Component({
  selector: 'app-contratos',
  templateUrl: './contratos.component.html',
  styleUrls: ['./contratos.component.css']
})
export class ContratosComponent implements OnInit {

  constructor(private serviciosService: ServiciosService,
    private habilidadesService: HabilidadesService,
    private contratosService: ContratosService,
    private inmuebleService: InmueblesService,
    private openPayService: OpenPayService,
    private authService: AuthService,
    private empleadosService: EmpleadosService,
    private scriptsLoad: ScriptsloadService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private router: Router) { }

  ngOnInit() {
    this.verificarSesion();
  }
  //-------------------------------VARIABLES----------------------------------------------
  user:any=null;
  servidor: string = GlobalConstants.nodeURL;
  contratos:any=null;
  showModalContrato:boolean=false;
  idContratoSeleccionado:any;
  detalleContrato:any=null;
//-------------------------------./VARIABLES----------------------------------------------
  
  private async verificarSesion() {
    console.log('Verificando', 'Tipo de usuario');
    var token = this.authService.getToken();

    if (this.authService.getToken() != null){
      this.authService.getTipoUsuario().subscribe(res => {
        let x: any = res;

        if (x == "EXPIRO") {
          this.toastr.info("Su sesion expiro");
          this.router.navigateByUrl('/auth/login');
        }
        if (token != null) {
          if (res[0].rol == "ADMINISTRADOR" || res[0].rol == "EMPLEADO"  && token != null) {
            this.user = res[0];
            console.log(this.user);
            this.getContratos();
            //console.log(res.dataUser.accessToken);
          }
        }
      }, err => {
        console.log(err);
      });
    }else{
        this.toastr.info("Inicie sesión")
        this.router.navigateByUrl('/auth/login');
      }
  }
  getContratos(){
    this.empleadosService.getContratosDeEmpleados(this.user.id_usuario).subscribe(
      res=>{
        console.log(res);
        this.contratos=res;
      }
    );
  }
  abrirModalContrato(id){
    this.idContratoSeleccionado=id;
    this.contratosService.detalleContrato(id).subscribe(
      res => {
        console.log(res);
        this.detalleContrato = res;
        this.showModalContrato = true;
      }
    );
    
  }
  cerrarModalContrato(){
    this.showModalContrato=false;
    
  }
  logout() {
    this.authService.logout();
    this.router.navigateByUrl('/auth/login');
    this.user = null;
  }
}
