import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ContratosComponent} from './contratos/contratos.component';
const routes: Routes = [
  { path: '', redirectTo: '/socio/principal', pathMatch: 'full' },
  { path: 'contratos', component: ContratosComponent },
  //{ path: 'contratos', component: ContratosComponent },
  //{ path: 'empleados', component: EmpleadosComponent },
  //{ path: 'servicios', component: ServiciosComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SocioRoutingModule { }
