import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { GlobalConstants } from 'src/app/models/constantes';
import { ServiciosService } from '../../services/servicios.service';
import { HabilidadesService } from '../../services/habilidades.service';
import { InmueblesService } from '../../services/inmuebles.service';
import { ContratosService } from '../../services/contratos.service';
import { AuthService } from '../../services/auth.service';
import { ScriptsloadService } from '../../services/scriptsload.service';
import { OpenPayService } from '../../services/openPay.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { chargeOP } from 'src/app/models/chargeOP';
import { CustomerOP } from 'src/app/models/customerOP';
import { CardOP } from 'src/app/models/cardsOP';
@Component({
  selector: 'app-contratos',
  templateUrl: './contratos.component.html',
  styleUrls: ['./contratos.component.css']
})
export class ContratosComponent implements OnInit {

  constructor(private serviciosService: ServiciosService,
    private habilidadesService: HabilidadesService,
    private contratosService: ContratosService,
    private inmuebleService: InmueblesService,
    private openPayService: OpenPayService,
    private authService: AuthService,
    private scriptsLoad: ScriptsloadService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private router: Router) { }

  servidor: string = GlobalConstants.nodeURL;
  user: any = null;
  contratos_activos: any = null;
  contratos_proximos: any = null;
  contratos_historial: any = null;
  contratosSeleccionados = [];
  tituloContrato:string="Contratos Activos:";
  detalleContrato: any = null;
  showModalContrato: boolean = false;
  ngOnInit() {
    this.verificarSesion();
  }

  private async verificarSesion() {
    console.log('Verificando', 'Tipo de usuario');
    var token = this.authService.getToken();

    if (this.authService.getToken() != null){
      this.authService.getTipoUsuario().subscribe(res => {
        let x: any = res;

        if (x == "EXPIRO") {
          this.toastr.info("Su sesion expiro");
          this.router.navigateByUrl('/auth/login');
        }
        if (token != null) {
          if (res[0].rol == "ADMINISTRADOR" && token != null) {
            this.user = res[0];
            console.log(this.user);
            this.getContratos();
          }
        }
      }, err => {
        console.log(err);
      });
    }else{
      this.toastr.info("Inicie sesión")
      this.router.navigateByUrl('/auth/login');
    }
  }

  getContratos() {
    this.contratosService.getContratosActivos().subscribe(
      res => {
        this.contratos_activos = res;
        this.contratosSeleccionados = res;
        console.log(res);
      }
    );
    this.contratosService.getContratosHistorial().subscribe(
      res => {
        this.contratos_historial = res;
        console.log(res);
      }
    );
    this.contratosService.getContratosProximos().subscribe(
      res => {
        this.contratos_proximos = res;
        console.log(res);
      }
    );
  }
  seleccionarContratos(contrato) {
    switch (contrato) {
      case "Activos":
        this.contratosSeleccionados = this.contratos_activos;
        this.tituloContrato="Contratos Activos:";
        break;
      case "Proximos":
        this.contratosSeleccionados = this.contratos_proximos;
        this.tituloContrato="Contratos Proximos:";
        break;
      case "Historial":
        this.contratosSeleccionados = this.contratos_historial;
        this.tituloContrato="Contratos Historial:";
        break;
    }
  }
  abrirModalContrato(id) {
    this.contratosService.detalleContrato(id).subscribe(
      res => {
        console.log(res);
        this.detalleContrato = res;
        this.showModalContrato = true;
      }
    );
  }
  cerrarModalContrato() {
    this.showModalContrato = false;
  }
  logout() {
    this.authService.logout();
    this.router.navigateByUrl('/auth/login');
    this.user = null;
  }
}
