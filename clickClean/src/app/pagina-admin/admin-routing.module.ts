import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PrincipalComponent} from './principal/principal.component';
import {ContratosComponent} from './contratos/contratos.component';
import {EmpleadosComponent} from './empleados/empleados.component';
import {ServiciosComponent} from './servicios/servicios.component';
const routes: Routes = [
  { path: '', redirectTo: '/admin/principal', pathMatch: 'full' },
  { path: 'principal', component: PrincipalComponent },
  { path: 'contratos', component: ContratosComponent },
  { path: 'empleados', component: EmpleadosComponent },
  { path: 'servicios', component: ServiciosComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
