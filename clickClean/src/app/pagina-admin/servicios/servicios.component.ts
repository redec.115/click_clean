import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { GlobalConstants } from 'src/app/models/constantes';
import { ServiciosService } from '../../services/servicios.service';
import { HabilidadesService } from '../../services/habilidades.service';
import { InmueblesService } from '../../services/inmuebles.service';
import { ContratosService } from '../../services/contratos.service';
import { AuthService } from '../../services/auth.service';
import { ImagenesService } from '../../services/imagenes.service';
import { ScriptsloadService } from '../../services/scriptsload.service';
import { OpenPayService } from '../../services/openPay.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { chargeOP } from 'src/app/models/chargeOP';
import { CustomerOP } from 'src/app/models/customerOP';
import { CardOP } from 'src/app/models/cardsOP';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatTableDataSource } from '@angular/material/table';
@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.component.html',
  styleUrls: ['./servicios.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ServiciosComponent implements OnInit {

  constructor(private serviciosService: ServiciosService,
    private habilidadesService: HabilidadesService,
    private contratosService: ContratosService,
    private inmuebleService: InmueblesService,
    private openPayService: OpenPayService,
    private authService: AuthService,
    private scriptsLoad: ScriptsloadService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private imagenService: ImagenesService,
    private router: Router) { }

  myName = new FormControl('', [
    Validators.required,
  ]);
  myDesc = new FormControl('', [
    Validators.required,
  ]);
  myPrice = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9.]*$')
  ]);

  myNameEditaH = new FormControl('', [
    Validators.required,
  ]);
  myDescEditaH = new FormControl('', [
    Validators.required,
  ]);
  myPriceEditaH = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9.]*$')
  ]);
  myNameEditaServ = new FormControl('', [
    Validators.required,
  ]);
  myDescEditaServ = new FormControl('', [
    Validators.required,
  ]);
  myNameNuevoServ = new FormControl('', [
    Validators.required,
  ]);
  myDescNuevoServ = new FormControl('', [
    Validators.required,
  ]);
  ngOnInit() {
    this.verificarSesion();
    this.getServicios();
    this.FormGroupEditaHabilidad = this.formBuilder.group({
      nombre_habilidad: this.myNameEditaH,
      descripcion_habilidad: this.myDescEditaH,
      precio_habilidad: this.myPriceEditaH
    });
    this.FormGroupEditaServicio = this.formBuilder.group({
      nombre_servicio: this.myNameEditaServ,
      caracteristicas_servicio: this.myDescEditaServ,
    });
    this.FormGroupNuevoServicio = this.formBuilder.group({
      nombre_servicio: this.myNameNuevoServ,
      caracteristicas_servicio: this.myDescNuevoServ,
    });
    this.FormGroupRegister = this.formBuilder.group({
      nombre: this.myName,
      descripcion: this.myDesc,
      precio: this.myPrice
    });
  }
  user: any = null;
  FormGroupRegister: FormGroup;
  FormGroupEditaHabilidad: FormGroup;
  FormGroupEditaServicio: FormGroup;
  FormGroupNuevoServicio: FormGroup;
  imagen_servicio: any = "";
  servicioSeleccionado: any;
  servidor: string = GlobalConstants.nodeURL;
  servicios: any = null;
  dataSource: any;
  detalleServicio = null;
  idServicioSeleccionado = 0;
  showModalServicio = false;
  showModalNuevoServicio = false;
  columnsToDisplay = ['id_servicio',
    'nombre_servicio', 'costo_servicio'];
  agregarHabilidad = false;
  editarServicio = false;
  checkList: any = null;
  id_edicionHabilidad = 0;
  editarHabildad = false;
  fileToUpload: File = null;
  fileToUploadNuevoS: File = null;
  imagePath1: any;
  imgURL1: any;
  imageName: string = "Seleccione una imagen";
  expandedElement: any | null;
  habilidadesFaltantes: any = null;
  total: number = 0;
  private async verificarSesion() {
    console.log('Verificando', 'Tipo de usuario');
    var token = this.authService.getToken();

    if (this.authService.getToken() != null){
      this.authService.getTipoUsuario().subscribe(res => {
        let x: any = res;

        if (x == "EXPIRO") {
          this.toastr.info("Su sesion expiro");
          this.router.navigateByUrl('/auth/login');
        }
        if (token != null) {
          if (res[0].rol == "ADMINISTRADOR"  && token != null) {
            this.user = res[0];
            console.log(this.user);
            //console.log(res.dataUser.accessToken);
          }
        }
      }, err => {
        console.log(err);
      });
    }else{
        this.toastr.info("Inicie sesión")
        this.router.navigateByUrl('/auth/login');
      }
  }

  getServicios() {
    this.serviciosService.obtenerServicios().subscribe(
      res => {
        this.servicios = res;
        console.log(res);
        this.dataSource = new MatTableDataSource(this.servicios);
      }
    );
  }
  getDetalleServicios(id, servicio) {
    this.servicioSeleccionado = servicio;
    this.idServicioSeleccionado = id;

    this.imagen_servicio = servicio.imagen_servicio;
    this.myNameEditaServ.setValue(servicio.nombre_servicio);
    this.myDescEditaServ.setValue(servicio.caracteristicas_servicio);

    this.serviciosService.habilidadesDeServicio(id).subscribe(
      res => {
        this.detalleServicio = res;
        console.log(res);
        let n = 0;
        this.total = 0;
        while (n < this.detalleServicio.length) {
          this.total += parseFloat(this.detalleServicio[n].precio_habilidad);
          n++;
        }
        this.getHabilidadesFaltantes();
        this.showModalServicio = true;
      }
    );
  }
  getHabilidadesFaltantes() {
    this.habilidadesService.obtenerHabilidades().subscribe(
      res => {
        this.habilidadesFaltantes = res;
        let n = 0;
        while (n < this.detalleServicio.length) {
          let j = 0;
          while (j < this.habilidadesFaltantes.length) {
            if (this.habilidadesFaltantes[j].id_habilidad == this.detalleServicio[n].id_habilidad) {
              this.habilidadesFaltantes.splice(j, 1);
              break;
            }
            j++;
          }
          n++;
        }

        let i = 0;
        this.checkList = [];
        while (i < this.habilidadesFaltantes.length) {
          let newh = {
            id_habilidad: this.habilidadesFaltantes[i].id_habilidad,
            nombre_habilidad: this.habilidadesFaltantes[i].nombre_habilidad,
            descripcion_habilidad: this.habilidadesFaltantes[i].descripcion_habilidad,
            precio_habilidad: this.habilidadesFaltantes[i].precio_habilidad,
            check: false
          };
          this.checkList[i] = newh;
          i++;
        }
      }
    );
  }
  checkHabilidad(id_habilidad) {
    let newHS = {
      id_servicio: this.idServicioSeleccionado,
      id_habilidad: id_habilidad
    };
    this.serviciosService.agregarHabilidadServicio(newHS).subscribe(
      res => {
        this.getServicios();
        this.getDetalleServicios(this.idServicioSeleccionado, this.servicioSeleccionado);
        this.toastr.success("Habilidad agregada");
      }
    );
  }
  registrarHabilidad(form) {
    console.log(form);
    if (form.status == "VALID") {
      this.habilidadesService.crearHabilidad(form.value).subscribe(
        res => {
          let newHabilidad = res[0];
          console.log(newHabilidad);
          let newHS = {
            id_servicio: this.idServicioSeleccionado,
            id_habilidad: newHabilidad.id_habilidad
          };
          this.serviciosService.agregarHabilidadServicio(newHS).subscribe(
            res => {
              this.getServicios();
              this.getDetalleServicios(this.idServicioSeleccionado, this.servicioSeleccionado);
              this.mostrarDetalle();
              this.toastr.success("Habilidad agregada");
            }
          );
        }
      );
    } else {
      this.toastr.warning("Rellene correctamente los datos", "Error");
    }
  }
  eliminarHabilidad(id_habilidad) {
    let delH = {
      id_servicio: this.idServicioSeleccionado,
      id_habilidad: id_habilidad
    };
    this.serviciosService.eliminarHabilidadServicio(delH).subscribe(
      res => {
        this.getServicios();
        this.getDetalleServicios(this.idServicioSeleccionado, this.servicioSeleccionado);
        this.mostrarDetalle();
        this.toastr.success("Habilidad eliminada");
      }
    );
  }
  habilitarServicio(id) {
    this.serviciosService.deshabilitarServicio(id).subscribe(
      res => {
        this.getServicios();
        this.toastr.success("Estado cambiado");
      }
    );
  }
  mostrarEditarHabilidad(habilidad) {
    this.id_edicionHabilidad = habilidad.id_habilidad;
    this.myNameEditaH.setValue(habilidad.nombre_habilidad);
    this.myPriceEditaH.setValue(habilidad.precio_habilidad);
    this.myDescEditaH.setValue(habilidad.descripcion_habilidad);
    console.log(this.FormGroupEditaHabilidad);
    this.editarHabildad = true;
  }
  editarHabilidad(form) {
    let editH = {
      id_habilidad: this.id_edicionHabilidad,
      nombre_habilidad: form.value.nombre_habilidad,
      descripcion_habilidad: form.value.descripcion_habilidad,
      precio_habilidad: form.value.precio_habilidad
    };
    if (form.status == "VALID") {
      this.habilidadesService.editarHabilidad(editH).subscribe(
        res => {
          this.toastr.success("Editado con exito");
        }, err => {
          this.toastr.error(err, "Error");
        }
      );
    }
  }
  editarServ(form) {
    let editS;

    if (this.fileToUpload != null) {
      editS = {
        id_servicio: this.idServicioSeleccionado,
        nombre_servicio: form.value.nombre_servicio,
        caracteristicas_servicio: form.value.caracteristicas_servicio,
        imagen_servicio: this.fileToUpload.name
      };
      const fd = new FormData();
      fd.append('file', this.fileToUpload, this.fileToUpload.name);
      this.imagenService.guardarImagen(fd).subscribe(
        res => {

        }
      );
    } else {
      editS = {
        id_servicio: this.idServicioSeleccionado,
        nombre_servicio: form.value.nombre_servicio,
        caracteristicas_servicio: form.value.caracteristicas_servicio,
        imagen_servicio: this.imagen_servicio
      };
    }
    if (form.status == "VALID") {
      this.serviciosService.editarServicio(editS).subscribe(
        res => {
          this.toastr.success("Editado con exito");
          this.imgURL1 = null;
          this.fileToUpload = null;
          this.getServicios();
          this.getDetalleServicios(this.idServicioSeleccionado, this.servicioSeleccionado);
          this.mostrarDetalle();
        }, err => {
          this.toastr.error(err, "Error");
        }
      );
    }
  }
  nuevoServicio(form) {
    
    if (form.status == "VALID" && this.fileToUploadNuevoS != null) {
      this.toastr.info("Espere un momento", "Agregando Servicio");
      let newS = {
        nombre_servicio: form.value.nombre_servicio,
        caracteristicas_servicio: form.value.caracteristicas_servicio,
        imagen_servicio: this.fileToUploadNuevoS.name
      }
      this.serviciosService.crearServicio(newS).subscribe(
        res => {
          this.servicioSeleccionado = res[0];
          console.log(this.servicioSeleccionado);
          this.idServicioSeleccionado = res[0].id_servicio;
          const fd = new FormData();
          let formato = this.servicioSeleccionado.imagen_servicio.split(".", 2); 
          fd.append('file', this.fileToUploadNuevoS, "serv_"+this.idServicioSeleccionado+"."+formato[1]);
          this.imagenService.guardarImagen(fd).subscribe(
            res => {
              
            }
          );
          let finalImg={
            id_servicio:this.idServicioSeleccionado,
            imagen_servicio: "serv_"+this.idServicioSeleccionado+"."+formato[1]
          };
          this.serviciosService.agregarImagenServicio(finalImg).subscribe(
            res=>{
              this.toastr.success("Agregue habilidades en la tabla de servicios", "Servicio agregado");
              this.getServicios();
              this.getDetalleServicios(this.idServicioSeleccionado, this.servicioSeleccionado);
              this.mostrarDetalle();
              this.fileToUploadNuevoS = null;
            }
          );
          
        }
      )
    }else{
      this.toastr.warning("Agregue imagenes y datos solicitados");
    }
  }
  handleFileInput(files: FileList) {

    this.imageName = files.item(0).name;
    //this.fileToUpload = files.item(0);
    this.fileToUpload = new File([files.item(0)], "serv_" + this.idServicioSeleccionado + "." + files.item(0).type.substring(6));
    console.log(this.fileToUpload);
    this.guardarTemporal(files);
  }
  handleFileInputNuevo(files: FileList) {

    this.imageName = files.item(0).name;
    //this.fileToUpload = files.item(0);
    this.fileToUploadNuevoS = new File([files.item(0)], "servicioRender" + "." + files.item(0).type.substring(6));
    console.log(this.fileToUploadNuevoS);
    this.guardarTemporal(files);
  }
  guardarTemporal(files) {
    console.log(files);
    if (files.length === 0)
      return;

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {

      return;
    }

    var reader = new FileReader();
    this.imagePath1 = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {

      this.imgURL1 = reader.result;
    }

  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  mostrarAgregar() {
    if(!this.agregarHabilidad){
      this.agregarHabilidad = true;
    this.editarServicio = false;
    }else{
      this.agregarHabilidad = false;
      this.editarServicio = false;
    }
    
  }
  mostrarDetalle() {
    this.agregarHabilidad = false;
    this.editarServicio = false;
    this.showModalNuevoServicio = false;
  }
  mostrarNuevoServicio() {
    this.showModalNuevoServicio = true;
  }
  mostrarEditarServicio() {

    if (!this.editarServicio)
      this.editarServicio = true;
    else
      this.editarServicio = false;
  }
  cerrarModalServicio() {
    this.showModalServicio = false;
    this.agregarHabilidad = false;
    this.imgURL1=null;
    this.fileToUpload=null;
  }
  cerrarModalNuevoServicio() {
    this.showModalNuevoServicio = false;
    this.imgURL1=null;
    this.fileToUploadNuevoS=null;
  }
  logout() {
    this.authService.logout();
    this.router.navigateByUrl('/auth/login');
    this.user = null;
  }

}
