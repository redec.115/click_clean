import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { GlobalConstants } from 'src/app/models/constantes';
import { ServiciosService } from '../../services/servicios.service';
import { HabilidadesService } from '../../services/habilidades.service';
import { InmueblesService } from '../../services/inmuebles.service';
import { ContratosService } from '../../services/contratos.service';
import { AuthService } from '../../services/auth.service';
import { ScriptsloadService } from '../../services/scriptsload.service';
import { OpenPayService } from '../../services/openPay.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { chargeOP } from 'src/app/models/chargeOP';
import { CustomerOP } from 'src/app/models/customerOP';
import { CardOP } from 'src/app/models/cardsOP';
@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {
  user: any = null;
  constructor(private serviciosService: ServiciosService,
    private habilidadesService: HabilidadesService,
    private contratosService: ContratosService,
    private inmuebleService: InmueblesService,
    private openPayService: OpenPayService,
    private authService: AuthService,
    private scriptsLoad: ScriptsloadService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private router: Router) { }

    servidor: string = GlobalConstants.nodeURL;
  ngOnInit() {
    this.verificarSesion();
  }

  private async verificarSesion() {
    console.log('Verificando', 'Tipo de usuario');
    var token = this.authService.getToken();

    if (this.authService.getToken() != null){
      this.authService.getTipoUsuario().subscribe(res => {
        let x: any = res;
        console.log(res);
        if (x == "EXPIRO") {
          this.toastr.info("Su sesion expiro");
          this.router.navigateByUrl('/auth/login');
        }
        else if (token != null) {
          
          if (res[0].rol == "ADMINISTRADOR" && token != null) {
            this.user = res[0];
            console.log(this.user);
            //console.log(res.dataUser.accessToken);
          }
        }
      }, err => {
        console.log(err);
      });
    }else{
      this.toastr.info("Inicie sesión")
      this.router.navigateByUrl('/auth/login');
    }
  }

  logout() {
    this.authService.logout();
    this.router.navigateByUrl('/auth/login');
    this.user = null;
  }
}
