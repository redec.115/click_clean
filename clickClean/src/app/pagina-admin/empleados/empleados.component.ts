import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { GlobalConstants } from 'src/app/models/constantes';
import { ServiciosService } from '../../services/servicios.service';
import { HabilidadesService } from '../../services/habilidades.service';
import { InmueblesService } from '../../services/inmuebles.service';
import { ContratosService } from '../../services/contratos.service';
import { AuthService } from '../../services/auth.service';
import { ScriptsloadService } from '../../services/scriptsload.service';
import { OpenPayService } from '../../services/openPay.service';
import { EmpleadosService } from '../../services/empleados.service';
import { ClientesService } from '../../services/clientes.service';
import { ImagenesService } from '../../services/imagenes.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { chargeOP } from 'src/app/models/chargeOP';
import { CustomerOP } from 'src/app/models/customerOP';
import { CardOP } from 'src/app/models/cardsOP';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatTableDataSource } from '@angular/material/table';
@Component({
  selector: 'app-empleados',
  templateUrl: './empleados.component.html',
  styleUrls: ['./empleados.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class EmpleadosComponent implements OnInit {

  constructor(private serviciosService: ServiciosService,
    private habilidadesService: HabilidadesService,
    private contratosService: ContratosService,
    private inmuebleService: InmueblesService,
    private openPayService: OpenPayService,
    private empleadosService: EmpleadosService,
    private authService: AuthService,
    private clientesService: ClientesService,
    private scriptsLoad: ScriptsloadService,
    private imagenService: ImagenesService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private router: Router) { }
  myPassword = new FormControl('', [
    Validators.required,
    Validators.pattern('[a-zA-Z0-9]{8,}') // <-- Here's how you pass in the custom validator using regex.
  ]);
  myPassword2 = new FormControl('', [
    Validators.required,
    Validators.pattern('[a-zA-Z0-9]{8,}') // <-- Here's how you pass in the custom validator using regex.
  ]);
  myEmail = new FormControl('', [
    Validators.required,
    Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')
  ]);
  myCel = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9]{10}$')
  ]);
  myTel = new FormControl('', [
    Validators.required
  ]);
  myName = new FormControl('', [
    Validators.required,
    Validators.pattern('^[a-zA-Z ñÑ]*$')
  ]);
  mySecondName = new FormControl('', [
    Validators.required,
    Validators.pattern('^[a-zA-Z ñÑ]*$')
  ]);
  mySecondName2 = new FormControl('', [
    Validators.pattern('^[a-zA-Z ñÑ]*$')
  ]);
  myDate = new FormControl('', [
    Validators.required,
  ]);
  FormGroupNuevoEmpleado: FormGroup;
  ngOnInit() {
    this.verificarSesion();
    this.FormGroupNuevoEmpleado = this.formBuilder.group({
      nombre: this.myName,
      ap_materno: this.mySecondName,
      ap_paterno: this.mySecondName2,
      telefono: this.myTel,
      celular: this.myCel,
      fecha_nac: this.myDate,
      email: this.myEmail,
      password: this.myPassword,
      password2: this.myPassword2,
    });
  }
  user: any = null;
  empleados: any = null;
  habilidades: any = null;
  dataSource: any;
  columnsToDisplay = ['id_usuario',
    'nombre_usuario', 'celular_usuario',
    'telcasa_usuario', 'ciudad_usuario'];
  expandedElement: any | null;
  showModalHabilidades = false;
  showModalNuevoEmpleado = false;
  habilidadesEmpleado = null;
  checkList = null;
  imageName: any = "Seleccione una imagen";
  fileToUpload: File = null;
  imagePath1: any;
  imgURL1: any;
  idEmpleadoSeleccionado = 0;
  private async verificarSesion() {
    console.log('Verificando', 'Tipo de usuario');
    var token = this.authService.getToken();

    if (this.authService.getToken() != null){
      this.authService.getTipoUsuario().subscribe(res => {
        let x: any = res;

        if (x == "EXPIRO") {
          this.toastr.info("Su sesion expiro");
          this.router.navigateByUrl('/auth/login');
        }
        if (token != null) {
          if (res[0].rol == "ADMINISTRADOR" && token != null) {
            this.user = res[0];
            this.getEmpleados();
          }
        }
      }, err => {
        console.log(err);
      });
    }else{
      this.toastr.info("Inicie sesión")
      this.router.navigateByUrl('/auth/login');
    }
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getEmpleados() {
    this.empleadosService.obtenerEmpleados().subscribe(
      res => {
        console.log(res);
        this.empleados = res;
        this.dataSource = new MatTableDataSource(this.empleados);
        this.getHabilidades();
      }
    );
  }
  getHabilidades() {
    this.habilidadesService.obtenerHabilidades().subscribe(
      res => {
        this.habilidades = res;
      }
    );
  }
  abrirModalHabilidades(id) {
    this.idEmpleadoSeleccionado = id;
    this.habilidadesService.habilidadDeEmpleado(id).subscribe(
      res => {
        this.habilidadesEmpleado = res;
        console.log(res);
        this.checkList = [];
        let i = 0;
        while (i < this.habilidades.length) {
          let found = this.habilidadesEmpleado.find(element =>
            element.id_habilidad == this.habilidades[i].id_habilidad,
          );
          if (found != null) {
            this.habilidades.splice(i, 1);
            i--;
          }

          i++;

        }
        i = 0;
        while (i < this.habilidades.length) {
          let newh = {
            id_habilidad: this.habilidades[i].id_habilidad,
            nombre_habilidad: this.habilidades[i].nombre_habilidad,
            descripcion_habilidad: this.habilidades[i].descripcion_habilidad,
            check: false
          };
          this.checkList[i] = newh;
          i++;
        }
        this.getHabilidades();
        this.showModalHabilidades = true;
      }
    );
  }
  quitarHabilidad(id) {
    let removeH = {
      id_empleado: this.idEmpleadoSeleccionado,
      id_habilidad: id
    };
    this.habilidadesService.eliminarHabilidadEmpleado(removeH).subscribe(
      res => {

        this.abrirModalHabilidades(this.idEmpleadoSeleccionado);
        this.toastr.success("Habilidad removida");
      }
    );
  }
  checkHabilidad(id) {
    let data = {
      id_empleado: this.idEmpleadoSeleccionado,
      id_habilidad: id
    }
    this.habilidadesService.agregarHabilidadEmpleado(data).subscribe(
      res => {
        this.abrirModalHabilidades(this.idEmpleadoSeleccionado);
      }
    );
  }
  nuevoEmpleado(form) {
    console.log(form);

    if (form.status == 'VALID' && this.fileToUpload != null) {
      if (form.value.password == form.value.password2) {
        console.log(form.value);
        this.cerrarModalNuevoEmpleado();
        let fecha: Date = new Date(form.value.fecha_nac);
        let mes;
        let dia;
        if ((fecha.getMonth() + 1) < 10)
          mes = 0 + "" + (fecha.getMonth() + 1);
        else
          mes = (fecha.getMonth() + 1);
        if ((fecha.getDate()) < 10)
          dia = 0 + "" + (fecha.getDate());
        else
          dia = (fecha.getDate());

        console.log(fecha.getFullYear() + "-" + mes + "-" + dia);
        let data: any = {
          ap_materno_usuario: form.value.ap_materno,
          ap_paterno_usuario: form.value.ap_paterno,
          celular_usuario: form.value.celular,
          email_usuario: form.value.email,
          email: form.value.email,//este es para el login
          fecha_nacimiento_usuario: fecha.getFullYear() + "-" + mes + "-" + dia,
          nombre_usuario: form.value.nombre,
          telcasa_usuario: form.value.telefono,
          id_rol: 2,
          password: form.value.password
        }
        console.log(data);
        this.authService.register(data, true).subscribe(
          res => {
            let newIdEmpleado = res[0].id_usuario;
            this.toastr.success("Empleado registrado");
            let formato = this.fileToUpload.name.split(".", 2);
            let newImage = {
              id_usuario: newIdEmpleado,
              imagen: "user_" + newIdEmpleado + formato[1]
            };
            this.clientesService.agregarImagenUsuario(newImage).subscribe(
              res => {
                const fd = new FormData();
                fd.append('file', this.fileToUpload, "user_" + newIdEmpleado + formato[1]);
                this.imagenService.guardarImagen(fd).subscribe(
                  res => {
                    this.fileToUpload = null;
                    this.imgURL1 = null;
                    window.location.reload();
                  }
                );
               
              }
            );
          }
        );

      } else {
        this.toastr.warning("Las contraseñas no coinciden", "Contraseña");
      }
    } else {
      this.toastr.warning("Llene sus datos correctamente");
    }
  }
  handleFileInput(files: FileList) {

    this.imageName = files.item(0).name;
    //this.fileToUpload = files.item(0);
    this.fileToUpload = new File([files.item(0)], "emp_" + 'temp' + "." + files.item(0).type.substring(6));
    console.log(this.fileToUpload);
    this.guardarTemporal(files);
  }
  guardarTemporal(files) {
    console.log(files);
    if (files.length === 0)
      return;

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {

      return;
    }

    var reader = new FileReader();
    this.imagePath1 = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {

      this.imgURL1 = reader.result;
    }

  }
  mostrarModalNuevoEmpleado() {
    this.showModalNuevoEmpleado = true;
  }
  cerrarModalNuevoEmpleado() {
    this.showModalNuevoEmpleado = false;
  }
  cerrarModalHabilidad() {
    this.showModalHabilidades = false;
  }
  logout() {
    this.authService.logout();
    this.user = null;
  }
}
