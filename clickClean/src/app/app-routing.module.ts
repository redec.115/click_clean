import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './pagina-principal/home/home.component';
const routes: Routes = [
  { path: '', component:HomeComponent },
  { path: 'index', redirectTo: '/', pathMatch: 'full' },
  { path: 'auth',  loadChildren: () => import('./pagina-auth/auth.module').then(m => m.AuthModule) },
  { path: 'client',  loadChildren: () => import('./pagina-cliente/cliente.module').then(m => m.ClientModule) },
  { path: 'admin', loadChildren: () => import('./pagina-admin/admin.module').then(m => m.AdminModule)},
  { path: 'socio', loadChildren: () => import('./pagina-socio/socio.module').then(m => m.SocioModule)},
  //{ path: 'index', loadChildren: './pagina-cliente/client.module#ClientModule'},
  //{ path: 'socio', loadChildren: './pagina-socio/socio.module#SocioModule'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
