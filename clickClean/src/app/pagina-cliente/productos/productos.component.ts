import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { GlobalConstants } from 'src/app/models/constantes';
import { ServiciosService } from '../../services/servicios.service';
import { HabilidadesService } from '../../services/habilidades.service';
import { InmueblesService } from '../../services/inmuebles.service';
import { ContratosService } from '../../services/contratos.service';
import { AuthService } from '../../services/auth.service';
import { ScriptsloadService } from '../../services/scriptsload.service';
import { OpenPayService } from '../../services/openPay.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { chargeOP } from 'src/app/models/chargeOP';
import { CustomerOP } from 'src/app/models/customerOP';
import { CardOP } from 'src/app/models/cardsOP';
import { MensajeService } from '../../services/mensaje.service';
@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {

  constructor(private serviciosService: ServiciosService,
    private habilidadesService: HabilidadesService,
    private contratosService: ContratosService,
    private inmuebleService: InmueblesService,
    private openPayService: OpenPayService,
    private authService: AuthService,
    private mensajeService: MensajeService,
    private scriptsLoad: ScriptsloadService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private router: Router) { }

  ngOnInit() {
    this.verificarSesion();
  }
  servidor: string = GlobalConstants.nodeURL;
  user: any = null;
  productos: any = null;
  carrito=0;
  private async verificarSesion() {
    console.log('Verificando', 'Tipo de usuario');
    var token = this.authService.getToken();

    if (this.authService.getToken() != null) {
      this.authService.getTipoUsuario().subscribe(res => {
        let x: any = res;

        if (x == "EXPIRO") {
          this.toastr.info("Su sesion expiro");
          this.router.navigateByUrl('/auth/login');
        }
        if (token != null) {
          if (res[0].rol == "ADMINISTRADOR" || res[0].rol == "EMPLEADO" || res[0].rol == "CLIENTE" && token != null) {
            this.user = res[0];
            console.log(this.user);
            this.getProductos();
            this.getCarrito();
          }
        }
      }, err => {
        console.log(err);
      });
    } else {
      this.toastr.info("Inicie sesión")
      this.router.navigateByUrl('/auth/login');
    }
  }
  getProductos() {
    this.serviciosService.obtenerProductos().subscribe(
      res => {
        console.log(res);
        this.productos = res;
      }
    );
  }
  getCarrito() {
    this.contratosService.consultarMiCarrito(this.user.id_usuario).subscribe(
      res => {
        console.log(res);
        this.carrito = res[0].length+ res[1].length;
        
      }
    );
  }
  agregarCarrito(producto,cant){
    let data={
      id_producto:producto,
      id_cliente:this.user.id_usuario,
      cantidad:cant
    }
    this.contratosService.agregarProductoCarrito(data).subscribe(
      res=>{
        console.log(res);
        this.toastr.success("Producto agregado al carrito");
        this.getCarrito();
      }
    )
  }
  logout() {
    this.authService.logout();
    this.user = null;
  }
}
