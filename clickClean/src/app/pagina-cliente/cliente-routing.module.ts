import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PerfilComponent} from './perfil/perfil.component';
import {ServiciosComponent} from './servicios/servicios.component';
import {CarritoComponent} from './carrito/carrito.component';
import { ContratosComponent } from './contratos/contratos.component';
import { ProductosComponent } from './productos/productos.component';
const routes: Routes = [
  { path: '', redirectTo: '/client/profile', pathMatch: 'full' },
  { path: 'profile', component: PerfilComponent },
  { path: 'service', component: ServiciosComponent },
  { path: 'cart', component: CarritoComponent },
  { path: 'contracts', component: ContratosComponent },
  { path: 'products', component: ProductosComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule { }
