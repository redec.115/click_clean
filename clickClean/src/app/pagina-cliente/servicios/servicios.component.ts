import { Component, OnInit } from '@angular/core';
import { GlobalConstants } from 'src/app/models/constantes';
import { ServiciosService } from '../../services/servicios.service';
import { HabilidadesService } from '../../services/habilidades.service';
import { ContratosService } from '../../services/contratos.service';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
interface services {
  [key: string]: any
  habilidades: any,
}

@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.component.html',
  styleUrls: ['./servicios.component.css']
})

export class ServiciosComponent implements OnInit {

  constructor(private serviciosService: ServiciosService,
    private habilidadesService: HabilidadesService,
    private contratosService: ContratosService,
    private authService: AuthService,
    private toastr: ToastrService,
    private router: Router) { }
  user: any = null;
  servidor: string = GlobalConstants.nodeURL;
  servicios: services;
  plantas: number = 1;
  habitaciones: number = 1;
  total :number= 0;
  carrito = 0;
  ngOnInit() {
    this.verificarSesion();
    this.getServicios();

  }
  private async verificarSesion() {
    console.log('Verificando', 'Tipo de usuario');
    var token = this.authService.getToken();

    if (this.authService.getToken() != null)
      this.authService.getTipoUsuario().subscribe(res => {
        let x: any = res;
        console.log(res);
        if (x == "EXPIRO") {
          this.toastr.info("Su sesion expiro");
          this.router.navigateByUrl('/auth/login');
        }
        if (token != null) {
          if (res[0].rol == "ADMINISTRADOR" || res[0].rol == "EMPLEADO" || res[0].rol == "CLIENTE" && token != null) {
            this.user = res[0];
            this.getCarrito();
            
            //console.log(res.dataUser.accessToken);
          }
        }
      }, err => {
        console.log(err);
      });
  }
  getServicios() {
    this.serviciosService.obtenerServicios().subscribe(
      res => {
        let ser = res;
        this.servicios = res;

        let i = 0;
        let j = 0;
        while (i < ser.length) {

          this.serviciosService.habilidadesDeServicio(ser[i].id_servicio).subscribe(
            res => {
              this.servicios[j].habilidades = res;
              j++;
            },
            (err) => {
              console.error(err)
            },
          );
          i++;
        }
        console.log(this.servicios);
      }
    );
  }
  getCarrito() {
    this.contratosService.consultarMiCarrito(this.user.id_usuario).subscribe(
      res => {
        console.log(res);
        this.total=0;
        this.carrito = res[0].length+ res[1].length;
        res[0].forEach(element => {
          this.total += parseFloat(element.costo_servicio);
        });
        res[1].forEach(element => {
          this.total += parseFloat(element.precio_producto) * parseFloat(element.cantidad_producto);
        });
      }
    );
  }
  cambioHab(event) {
    this.habitaciones = event.value;
  }
  cambioPla(event) {
    this.plantas = event.value;
  }
  agregarCarrito(id_se){
    let servicio:any={
      id_cliente:parseInt(this.authService.getID()),
      id_servicio:id_se
    };
    this.contratosService.agregarServicioCarrito(servicio).subscribe(
      res=>{
        this.getCarrito();
      }
    );
  }
  
  onRouter(pagina) {
    switch (pagina) {
      case "login":
        this.router.navigateByUrl("/auth/login");
        break;
      case "register":
        this.router.navigateByUrl("/auth/register");
        break;
      case "home":
        this.router.navigateByUrl("/index");
        break;
      case "services":
        this.router.navigateByUrl("/client/service");
        break;
      case "profile":
        this.router.navigateByUrl("/client/profile");
        break;
      case "cart":
        this.router.navigateByUrl("/client/cart");
        break;
    }
  }
  logout() {
    this.authService.logout();
    this.user = null;
  }
}
