import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { GlobalConstants } from 'src/app/models/constantes';
import { ServiciosService } from '../../services/servicios.service';
import { HabilidadesService } from '../../services/habilidades.service';
import { InmueblesService } from '../../services/inmuebles.service';
import { ContratosService } from '../../services/contratos.service';
import { AuthService } from '../../services/auth.service';
import { ScriptsloadService } from '../../services/scriptsload.service';
import { OpenPayService } from '../../services/openPay.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { chargeOP } from 'src/app/models/chargeOP';
import { CustomerOP } from 'src/app/models/customerOP';
import { CardOP } from 'src/app/models/cardsOP';
import { MensajeService } from '../../services/mensaje.service';
@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.css']
})
export class CarritoComponent implements OnInit {

  constructor(private serviciosService: ServiciosService,
    private habilidadesService: HabilidadesService,
    private contratosService: ContratosService,
    private inmuebleService: InmueblesService,
    private openPayService: OpenPayService,
    private authService: AuthService,
    private mensajeService: MensajeService,
    private scriptsLoad: ScriptsloadService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private router: Router,
  ) { }

  user: any = null;
  servidor: string = GlobalConstants.nodeURL;
  total = 0;
  carrito = 0;
  carritoServicios = 0;
  carritoProductos = 0;
  carritoItemsServicio: any = null;
  carritoItemsProducto: any = null;
  inmuebles: any = [];
  //timers

  time = { hour: 8, minute: 0 };
  time2 = { hour: 17, minute: 0 };
  minuteStep = 30;
  fechaInicio: Date;
  fechaFin: Date;
  meridian = true;
  minDate = new Date();
  minDate2 = new Date();
  myFilter = (d: Date | null): boolean => {
    const day = (d || new Date()).getDay();
    // Prevent Saturday and Sunday from being selected.
    return day !== 0;
  }
  range = new FormGroup({
    start: new FormControl('', [
      Validators.required,
    ]),
    end: new FormControl('', [
      Validators.required,
    ])
  });
  myDireccion = new FormControl('', [
    Validators.required,
  ]);
  myNumero = new FormControl('', [
    Validators.required,
  ]);
  myColonia = new FormControl('', [
    Validators.required,
  ]);
  myCiudad = new FormControl('', [
    Validators.required,
  ]);
  myEstado = new FormControl('', [
    Validators.required,
  ]);
  myTipo = new FormControl('', [
    Validators.required,
  ]);
  myTamano = new FormControl('', [
    Validators.required,
  ]);
  myPlantas = new FormControl('', [
    Validators.required,
  ]);
  myStart = new FormControl('', [
    Validators.required,
  ]);
  myEnd = new FormControl('', [
    Validators.required,
  ]);
  myTitular = new FormControl('', [
    Validators.required,
    Validators.pattern('^[a-zA-Z ñÑ]*$'),
  ]);
  myTarjeta = new FormControl('', [
    Validators.required,
    Validators.pattern('^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$')

  ]);
  myMonthExp = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9]{2}$'),
    Validators.maxLength(2)
  ]);
  myYearExp = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9]{2}$'),
    Validators.maxLength(2)
  ]);
  myCVV = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9]{3,4}$'),
    Validators.maxLength(4)
  ]);
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  cardFormGroup: FormGroup;
  states: any;
  cities: any;
  jsonMuni: any;
  deviceID: any;
  tarjetasRegistradas: any;
  tarjetasSeleccionada: any;
  registrarNuevaTarjeta: boolean = false;
  inmuebleSeleccionado: any;
  empleadosPotenciales: any = [];
  contratoCreado: any = null;
  pagando: boolean = false;
  ngOnInit() {
    this.verificarSesion();

    //this.loadScripts();
    this.firstFormGroup = this.formBuilder.group({
      colonia: this.myColonia,
      direccion: this.myDireccion,
      numero: this.myNumero,
      cuartos: this.myTamano,
      plantas: this.myPlantas,
      ciudad: this.myCiudad,
      estado: this.myEstado,
      tipo: this.myTipo
    });
    this.secondFormGroup = this.formBuilder.group({
      start: this.myStart,
      end: this.myEnd
    });
    this.cardFormGroup = this.formBuilder.group({
      titular: this.myTitular,
      tarjeta: this.myTarjeta,
      mes: this.myMonthExp,
      ano: this.myYearExp,
      cvv: this.myCVV
    });
    this.getTarjetas();
  }
  ngAfterViewInit() {
    //this.loadScripts();
    this.dataToArray();
    //console.log(OpenPay);
    this.deviceID = this.openPayService.initOpenPay();
    console.log(this.deviceID);
  }

  task: any = {
    name: 'Seleccionar todos los días',
    completed: false,
    color: 'primary',
    subtasks: [
      { name: 'Lunes', completed: false, color: 'primary', disabled: true },
      { name: 'Martes', completed: false, color: 'primary', disabled: true },
      { name: 'Miércoles', completed: false, color: 'primary', disabled: true },
      { name: 'Jueves', completed: false, color: 'primary', disabled: true },
      { name: 'Viernes', completed: false, color: 'primary', disabled: true },
      { name: 'Sábado', completed: false, color: 'primary', disabled: true },
      //{name: 'Domingo', completed: false, color: 'primary'},
    ]
  };

  allComplete: boolean = false;

  updateAllComplete() {
    this.allComplete = this.task.subtasks != null && this.task.subtasks.every(t => t.completed);
  }

  someComplete(): boolean {
    if (this.task.subtasks == null) {
      return false;
    }

    return this.task.subtasks.filter(t => t.completed).length > 0 && !this.allComplete;
  }

  setAll(completed: boolean) {
    this.allComplete = completed;
    if (this.task.subtasks == null) {
      return;
    }
    console.log(this.task);
    this.task.subtasks.filter(t => t.disabled == false).forEach(t => {
      t.completed = completed
    });
    //this.task.subtasks.forEach(t => t.completed = completed);
  }

  private loadScripts() {
    // You can load multiple scripts by just providing the key as argument into load method of the service
    this.scriptsLoad.load('municipios').then(data => {
      // Script Loaded Successfully
      this.scriptsLoad.load('estados_select').then(data => {
        // Script Loaded Successfully

      }).catch(error => console.log(error));
    }).catch(error => console.log(error));
  }
  private async verificarSesion() {
    console.log('Verificando', 'Tipo de usuario');
    var token = this.authService.getToken();

    if (this.authService.getToken() != null) {
      this.authService.getTipoUsuario().subscribe(res => {
        let x: any = res;

        if (x == "EXPIRO") {
          this.toastr.info("Su sesion expiro");
          this.router.navigateByUrl('/auth/login');
        }
        if (token != null) {
          if (res[0].rol == "ADMINISTRADOR" || res[0].rol == "EMPLEADO" || res[0].rol == "CLIENTE" && token != null) {
            this.user = res[0];
            console.log(this.user);
            this.getCarrito();
            this.getInmuebles();
            //console.log(res.dataUser.accessToken);
          }
        }
      }, err => {
        console.log(err);
      });
    } else {
      this.toastr.info("Inicie sesión")
      this.router.navigateByUrl('/auth/login');
    }
  }
  getInmuebles() {
    this.inmuebleService.inmueblesDeCliente(parseInt(this.authService.getID())).subscribe(
      res => {
        this.inmuebles = res;
      }
    );
  }
  getCarrito() {
    this.contratosService.consultarMiCarrito(this.user.id_usuario).subscribe(
      res => {
        this.total = 0;
        this.carritoItemsServicio = res[0];
        this.carritoItemsProducto = res[1];
        this.carritoProductos = 0;
        this.carritoServicios = 0;
        console.log(res);
        this.carrito = res[0].length + res[1].length;
        res[0].forEach(element => {
          this.carritoServicios += 1;
          this.total += parseFloat(element.costo_servicio);
        });
        res[1].forEach(element => {
          this.carritoProductos += 1;
          this.total += parseFloat(element.precio_producto) * parseFloat(element.cantidad_producto);
        });
        this.buscarEmpleadosPotenciales(0, this.empleadosPotenciales);
      }
    );
  }
  async readData() {
    // Creamos el objeto:
    let http = new XMLHttpRequest();
    // realizamos la petición:
    http.onreadystatechange = function () {
      if (this.readyState === this.DONE) {
        console.log(this.status) // do something; the request has completed
      }
    }
    http.open('GET', '../../../assets/js/municipios.json', false);
    // Enviamos la petición:
    http.send();
    // retornamos el valor de la petición:
    return http.response;
  }

  async dataToArray() {
    // Ponemos los datos en una variable:
    let data = await this.readData();
    // Convertimos los datos en objeto;
    let obj = JSON.parse(data);
    // Creamos un arreglo a partir del objeto:
    let arr = Object.values(obj);
    // Retornamos el arreglo:
    this.jsonMuni = arr;
    this.getStates();
    return arr;
  }

  getStates() {
    // Creamos un arreglo vacío para retornar:
    this.states = new Array();
    // Recorremos el arreglo de datos:
    this.jsonMuni.forEach(element => {
      // Añadimos la propiedad 'nombre' del elemento recorrido:
      this.states.push(element.nombre);
    });
    // Retornamos el arreglo resultante:
    //return states;
  }
  getCities(state) {
    // Aplicamos una busqueda al arreglo principal de datos:
    let found = this.jsonMuni.find(element =>
      // Retornamos el objeto del estado:
      element.nombre == state
    );
    // Retornamos el valor de la propiedad 'municipios':
    this.cities = found.municipios;
  }
  selectInmueble(id, event) {
    if (event.isUserInput) {
      let seleccionInmb: any = this.inmuebles.find(obj => obj.id_inmueble === id);
      this.inmuebleSeleccionado = seleccionInmb;

      this.myDireccion.setValue(seleccionInmb.calle_inm);
      this.myColonia.setValue(seleccionInmb.colonia_inm);
      this.myNumero.setValue(seleccionInmb.numero_inm);
      this.myTamano.setValue(seleccionInmb.tamano_inm);
      this.myPlantas.setValue(seleccionInmb.numero_plantas_inm);
      this.myTipo.setValue(seleccionInmb.tipo_inm);

      (document.getElementById("estado") as HTMLSelectElement).selectedIndex = seleccionInmb.estado_inm;

      this.getCities(seleccionInmb.estado_inm);

      (document.getElementById("municipio") as HTMLSelectElement).selectedIndex = seleccionInmb.ciudad_inm;
      this.myEstado.setValue(seleccionInmb.estado_inm);
      this.myCiudad.setValue(seleccionInmb.ciudad_inm);
    }
  }
  changeDate(form, input) {
    if (input == 1) {
      this.secondFormGroup.value.start = form.value;
      this.fechaInicio = form.value;
    } else if (input == 2) {
      this.secondFormGroup.value.end = form.value;
      this.fechaFin = form.value;

      if (this.fechaFin != null) {
        this, this.secondFormGroup.setValue({
          start: this.fechaInicio,
          end: this.fechaFin
        });
        this.diasDisponibles();
      }

    }
  }
  diasDisponibles() {
    let d = 0;
    this.task.subtasks[0].disabled = true;
    this.task.subtasks[1].disabled = true;
    this.task.subtasks[2].disabled = true;
    this.task.subtasks[3].disabled = true;
    this.task.subtasks[4].disabled = true;
    this.task.subtasks[5].disabled = true;

    this.task.subtasks[0].completed = false;
    this.task.subtasks[1].completed = false;
    this.task.subtasks[2].completed = false;
    this.task.subtasks[3].completed = false;
    this.task.subtasks[4].completed = false;
    this.task.subtasks[5].completed = false;
    //this.task.subtasks[6].disabled=false;

    this.updateAllComplete();

    let monthdayI = this.fechaInicio.getDate();
    let monthdayF = this.fechaFin.getDate();
    if (monthdayF - monthdayI < 6) {
      if (this.fechaFin.getDay() > this.fechaInicio.getDay()) {
        d = this.fechaInicio.getDay();

        while (d <= this.fechaFin.getDay()) {
          switch (d) {
            case 1:
              this.task.subtasks[0].disabled = false;
              break;
            case 2:
              this.task.subtasks[1].disabled = false;
              break;
            case 3:
              this.task.subtasks[2].disabled = false;
              break;
            case 4:
              this.task.subtasks[3].disabled = false;
              break;
            case 5:
              this.task.subtasks[4].disabled = false;
              break;
            case 6:
              this.task.subtasks[5].disabled = false;
              break;
            //case 7:
            //this.task.subtasks[6].disabled=false;
            //break;
          }
          d++;
        }


      } else {
        d = this.fechaFin.getDay();

        while (d <= this.fechaInicio.getDay()) {
          switch (d) {
            case 1:
              this.task.subtasks[0].disabled = false;
              break;
            case 2:
              this.task.subtasks[1].disabled = false;
              break;
            case 3:
              this.task.subtasks[2].disabled = false;
              break;
            case 4:
              this.task.subtasks[3].disabled = false;
              break;
            case 5:
              this.task.subtasks[4].disabled = false;
              break;
            case 6:
              this.task.subtasks[5].disabled = false;
              break;
            //case 7:
            //this.task.subtasks[6].disabled=false;
            //break;
          }
          d++;
        }

      }
    }
    //SI son mas de 5 dias habilita todo
    else {
      this.task.subtasks[0].disabled = false;
      this.task.subtasks[1].disabled = false;
      this.task.subtasks[2].disabled = false;
      this.task.subtasks[3].disabled = false;
      this.task.subtasks[4].disabled = false;
      this.task.subtasks[5].disabled = false;
    }
  }
  getTarjetas() {
    this.openPayService.getOpenPayIDCustomer(this.authService.getID()).subscribe(
      res => {
        let idOpenPay = "";
        if (res.length != 0) {
          idOpenPay = res[0].id_openpay;
          this.openPayService.getTarjetasCliente(idOpenPay).subscribe(
            res => {
              this.tarjetasRegistradas = res;
            }
          );
        }

      });
  }

  step(event) {
    switch (event.selectedIndex) {
      case 1:
        if (this.inmuebleSeleccionado != null) {
          if (this.firstFormGroup.status == "VALID") {
            if (this.inmuebleSeleccionado.calle_inm != this.myDireccion.value
              || this.inmuebleSeleccionado.ciudad_inm != this.myCiudad.value
              || this.inmuebleSeleccionado.colonia_inm != this.myColonia.value
              || this.inmuebleSeleccionado.estado_inm != this.myEstado.value
              || this.inmuebleSeleccionado.numero_inm != this.myNumero.value
              || this.inmuebleSeleccionado.tamano_inm != this.myTamano.value
              || this.inmuebleSeleccionado.numero_plantas_inm != this.myPlantas.value
              || this.inmuebleSeleccionado.tipo_inm != this.myTipo.value) {
              let newInmueble = {
                tipo: this.myTipo.value,
                tamano: this.myTamano.value,
                numero_plantas: this.myPlantas.value,
                calle: this.myDireccion.value,
                numero: this.myNumero.value,
                colonia: this.myColonia.value,
                ciudad: this.myCiudad.value,
                estado: this.myEstado.value,
                id_cliente: this.user.id_usuario

              }
              this.inmuebleService.crearInmueble(newInmueble).subscribe(
                res => {
                  this.inmuebleSeleccionado = res[0];
                }
              );
            }
          } else {
            this.toastr.warning("Escribe todos los datos de tu dirección", "Campos incompletos");
          }
        }
        else {
          if (this.firstFormGroup.status == "VALID") {
            let newInmueble = {
              tipo: this.myTipo.value,
              tamano: this.myTamano.value,
              numero_plantas: this.myPlantas.value,
              calle: this.myDireccion.value,
              numero: this.myNumero.value,
              colonia: this.myColonia.value,
              ciudad: this.myCiudad.value,
              estado: this.myEstado.value,
              id_cliente: this.user.id_usuario

            }
            this.inmuebleService.crearInmueble(newInmueble).subscribe(
              res => {
                this.inmuebleSeleccionado = res[0];
              }
            );
          } else {
            this.toastr.warning("Escribe todos los datos de tu dirección", "Campos incompletos");
          }
        }
        break;
      case 2:
        console.log(this.time);
        console.log(this.time2);
        if (this.secondFormGroup.status != "VALID") {
          this.toastr.warning("Seleccione las fechas", "Campos incompletos");
        }
        break;
    }

  }
  nuevaTarjeta() {
    if (!this.registrarNuevaTarjeta) {
      this.registrarNuevaTarjeta = true;
      this.tarjetasSeleccionada = null;
    }
    else
      this.registrarNuevaTarjeta = false;
  }
  seleccionarTarjeta(id) {
    this.tarjetasSeleccionada = id;
  }
  pagar(form) {
    this.pagando = true;
    if (this.firstFormGroup.status == "VALID") {
      console.log(this.task.subtasks.filter(t => t.completed).length);
      console.log(this.secondFormGroup);
      if (this.secondFormGroup.status == "VALID" && this.task.subtasks.filter(t => t.completed).length > 0) {

        this.toastr.info("Espera un momento", "Procesando Pago");

        this.openPayService.getOpenPayIDCustomer(this.authService.getID()).subscribe(
          res => {
            let idOpenPay = "";
            //si ya fue registrado como cliente
            if (res.length != 0) {
              idOpenPay = res[0].id_openpay;

              let newCardOp: CardOP = {
                card_number: form.value.tarjeta,
                cvv2: form.value.cvv,
                expiration_month: form.value.mes,
                expiration_year: form.value.ano,
                holder_name: form.value.titular,
                customer_id: idOpenPay
              }
              this.toastr.info("Espera un momento", "Validando Tarjeta");
              this.openPayService.crearTarjeta(newCardOp).subscribe(
                res => {
                  console.log(res);
                  if (res.error_code == null) {
                    let newSourceID = res.id;
                    let cargo: chargeOP = {
                      amount: this.total,
                      description: "Servicio de limpieza",
                      device_session_id: this.deviceID,
                      method: "card",
                      source_id: newSourceID,
                      customer_id: idOpenPay
                    }
                    this.openPayService.crearCargo(cargo).subscribe(
                      res => {

                        if (res.status == "completed") {
                          this.toastr.success("Pago realizado con exito", "Pago Exitoso");
                          this.crearServicio();
                        }
                        else {
                          this.pagando = false;
                          this.toastr.error(res.description, "Error code: " + res.error_code);
                        }
                      }
                    );
                  } else {
                    this.pagando = false;
                    this.toastr.error(res.description, "Error code: " + res.error_code);
                  }
                }
              );
            }
            else {
              let newCustomer: CustomerOP = {
                email: this.user.email_usuario,
                last_name: this.user.ap_materno_usuario + " " + this.user.ap_paterno_usuario,
                name: this.user.nombre_usuario,
                requires_account: false
              }
              this.openPayService.crearCliente(newCustomer).subscribe(
                res => {
                  let newCustomerID = res.id;
                  let newCardOp: CardOP = {
                    card_number: form.value.tarjeta,
                    cvv2: form.value.cvv,
                    expiration_month: form.value.mes,
                    expiration_year: form.value.ano,
                    holder_name: form.value.titular,
                    customer_id: newCustomerID
                  }
                  this.toastr.info("Espera un momento", "Validando Tarjeta");
                  this.openPayService.crearTarjeta(newCardOp).subscribe(
                    res => {
                      if (res.error_code == null) {
                        console.log(res);
                        let newSourceID = res.id;
                        let cargo: chargeOP = {
                          amount: this.total,
                          description: "Servicio de limpieza",
                          device_session_id: this.deviceID,
                          method: "card",
                          source_id: newSourceID,
                          customer_id: newCustomerID
                        }
                        this.openPayService.crearCargo(cargo).subscribe(
                          res => {
                            console.log(res);
                            if (res.status == "completed") {
                              this.toastr.success("Pago realizado con exito redireccionando espere...", "Pago Exitoso");
                              this.crearServicio();
                            }
                            else {
                              this.pagando = false;
                              this.toastr.error(res.description, "Error code: " + res.error_code);
                            }
                          }
                        );
                      } else {
                        this.pagando = false;
                        this.toastr.error(res.description, "Error code: " + res.error_code);
                      }
                    }
                  );
                }
              );
            }
          }
        );
      } else {
        this.pagando = false;
        this.toastr.warning("Seleccione fechas o días validos", "Error");
      }

    } else {
      this.pagando = false;
      this.toastr.warning("Llene los datos de dirrección", "Error");
    }
  }
  async pagarConTarjetaSeleccionada() {
    this.pagando = true;
    console.log(this.tarjetasSeleccionada)
    if (this.firstFormGroup.status == "VALID") {
      if (this.secondFormGroup.status == "VALID" && this.task.subtasks.filter(t => t.completed).length > 0) {
        this.toastr.info("Espera un momento", "Procesando Pago");
        this.openPayService.getOpenPayIDCustomer(this.user.id_usuario).subscribe(
          res => {
            let idCustomer = res[0].id_openpay;
            let cargo: chargeOP = {
              amount: this.total,
              description: "Servicio de limpieza",
              device_session_id: this.deviceID,
              method: "card",
              source_id: this.tarjetasSeleccionada,
              customer_id: idCustomer
            }
            console.log(cargo);
            this.openPayService.crearCargo(cargo).subscribe(
              async res => {
                console.log(res);
                if (res.status == "completed") {
                  this.toastr.success("Pago realizado con exito redireccionando espere...", "Pago Exitoso");
                  this.crearServicio();
                }
                else if (res.error_code != "") {
                  this.pagando = false;
                  this.toastr.error(res.description, "Error code: " + res.error_code);
                }


              }
            );
          }
        );
      } else {
        this.pagando = false;
        this.toastr.warning("Seleccione fechas o días validos", "Error");
      }

    } else {
      this.pagando = false;
      this.toastr.warning("Llene los datos de dirrección", "Error");
    }



  }

  async buscarEmpleadosPotenciales(index, empleados) {
    if (index < this.carritoItemsServicio.length) {
      await this.contratosService.getEmpleadosDisponiblesDeServicio(this.carritoItemsServicio[index].id_servicio).subscribe(
        async res => {
          let nuevaLista = res;
          //distinct
          let array3 = empleados.concat(nuevaLista);
          array3 = array3.filter((item, index) => {
            const _thing = JSON.stringify(item);
            return index === array3.findIndex(obj => {
              return JSON.stringify(obj) === _thing;
            });
          });

          index++;
          await this.buscarEmpleadosPotenciales(index, array3);
        }
      );
    } else {
      console.log(empleados);
      this.empleadosPotenciales = empleados;
    }


  }
  crearServicio() {
    let r = Math.random() * (this.empleadosPotenciales.length - 0) + 0;
    console.log(this.empleadosPotenciales);
    console.log(Math.floor(r));
    var hora_i: string;
    var hora_f: string;
    if (this.time.hour < 10 && this.time.minute < 10) {
      hora_i = "0" + this.time.hour + ":0" + this.time.minute + ":00";
    }
    else if (this.time.hour < 10 && this.time.minute >= 10) {
      hora_i = "0" + this.time.hour + ":" + this.time.minute + ":00";
    }
    else if (this.time.hour >= 10 && this.time.minute < 10) {
      hora_i = this.time.hour + ":0" + this.time.minute + ":00";
    }
    else {
      hora_i = this.time.hour + ":" + this.time.minute + ":00";
    }

    if (this.time2.hour < 10 && this.time2.minute < 10) {
      hora_f = "0" + this.time2.hour + ":0" + this.time2.minute + ":00";
    }
    else if (this.time2.hour < 10 && this.time2.minute >= 10) {
      hora_f = "0" + this.time2.hour + ":" + this.time2.minute + ":00";
    }
    else if (this.time2.hour >= 10 && this.time2.minute < 10) {
      hora_f = this.time2.hour + ":0" + this.time2.minute + ":00";
    }
    else {
      hora_f = this.time2.hour + ":" + this.time2.minute + ":00";
    }
    var fecha_i;
    if (this.fechaInicio.getMonth() < 10 && this.fechaInicio.getDate() < 10)
      fecha_i = this.fechaInicio.getFullYear() + "-0" + (this.fechaInicio.getMonth() + 1) + "-0" + this.fechaInicio.getDate() + " " + hora_i;
    else if (this.fechaInicio.getMonth() < 10)
      fecha_i = this.fechaInicio.getFullYear() + "-0" + (this.fechaInicio.getMonth() + 1) + "-" + this.fechaInicio.getDate() + " " + hora_i;
    else if (this.fechaInicio.getDate() < 10)
      fecha_i = this.fechaInicio.getFullYear() + "-" + (this.fechaInicio.getMonth() + 1) + "-0" + this.fechaInicio.getDate() + " " + hora_i;
    else
      fecha_i = this.fechaInicio.getFullYear() + "-" + (this.fechaInicio.getMonth() + 1) + "-" + this.fechaInicio.getDate() + " " + hora_i;

    var fecha_f;
    if (this.fechaFin.getMonth() < 10 && this.fechaFin.getDate() < 10)
      fecha_f = this.fechaFin.getFullYear() + "-0" + (this.fechaFin.getMonth() + 1) + "-0" + this.fechaFin.getDate() + " " + hora_f;
    else if (this.fechaFin.getMonth() < 10)
      fecha_f = this.fechaFin.getFullYear() + "-0" + (this.fechaFin.getMonth() + 1) + "-" + this.fechaFin.getDate() + " " + hora_f;
    else if (this.fechaFin.getDate() < 10)
      fecha_f = this.fechaFin.getFullYear() + "-" + (this.fechaFin.getMonth() + 1) + "-0" + this.fechaFin.getDate() + " " + hora_f;
    else
      fecha_f = this.fechaFin.getFullYear() + "-" + (this.fechaFin.getMonth() + 1) + "-" + this.fechaFin.getDate() + " " + hora_f;

    let newContrato = {
      id_cliente: this.user.id_usuario,
      id_inmueble: this.inmuebleSeleccionado.id_inmueble,
      id_empleado: this.empleadosPotenciales[Math.floor(r)].id_usuario,
      inicio_contrato: fecha_i,
      fin_contrato: fecha_f,
      lunes: this.task.subtasks[0].completed ? 1 : 0,
      martes: this.task.subtasks[1].completed ? 1 : 0,
      miercoles: this.task.subtasks[2].completed ? 1 : 0,
      jueves: this.task.subtasks[3].completed ? 1 : 0,
      viernes: this.task.subtasks[4].completed ? 1 : 0,
      sabado: this.task.subtasks[5].completed ? 1 : 0,
      domingo: 0,
      hora_inicio: hora_i,
      hora_fin: hora_f,
      total_contrato: this.total
    }
    this.contratosService.agregarContrato(newContrato).subscribe(
      res => {
        console.log(res);
        this.contratoCreado = res[0];
        this.agregarServiciosContrato(0);
      }
    );
  }

  agregarServiciosContrato(index) {
    if (index < this.carritoItemsServicio.length) {
      let data = {
        id_servicio: this.carritoItemsServicio[index].id_servicio,
        id_contrato: this.contratoCreado.id_contrato,
        total_servicio_contrato: this.carritoItemsServicio[index].costo_servicio
      }
      this.contratosService.agregarServicioContrato(data).subscribe(
        res => {
          index++;
          this.agregarServiciosContrato(index);
        }
      )
    } else {

      this.enviarMensaje();

    }
  }
  eliminarTarjeta() {
    this.openPayService.getOpenPayIDCustomer(this.user.id_usuario).subscribe(
      res => {
        let idCustomer = res[0].id_openpay;

        let data = {
          customer_id: idCustomer,
          card_id: this.tarjetasSeleccionada
        }
        this.openPayService.eliminarTarjeta(data).subscribe(
          res => {
            console.log(res);
            this.getTarjetas();
            this.tarjetasSeleccionada = null;
          }
        );
      });
  }
  eliminarCarritoServicio(id) {
    let data = {
      id_cliente: this.user.id_usuario,
      id_servicio: id
    };
    this.contratosService.eliminarServicioCarrito(data).subscribe(
      res => {
        console.log(res);
        this.getCarrito();
      }
    );
  }
  eliminarCarritoProducto(id) {
    let data = {
      id_cliente: this.user.id_usuario,
      id_producto: id
    };
    this.contratosService.eliminarProductoCarrito(data).subscribe(
      res => {
        console.log(res);
        this.getCarrito();
      }
    );
  }
  enviarMensaje() {

    let mensaje = {
      asunto: "Nuevo contrato",
      para: "nuevocontrato",
      destinatario: "redec.115@gmail.com",
      email: "p.ibarrag14@gmail.com",
      nombre: "ClickClean",

      servicio: this.carritoItemsServicio,

      cliente: this.user.nombre_usuario,
      telefono: this.user.celular_usuario,
      fecha_inicio: this.fechaInicio.toLocaleString(),
      fecha_fin: this.fechaFin.toLocaleString(),
      calle: this.inmuebleSeleccionado.calle_inm,
      numero_ext: this.inmuebleSeleccionado.numero_inm,
      municipio: this.inmuebleSeleccionado.ciudad_inm,
      estado: this.inmuebleSeleccionado.estado_inm,
    }
    this.mensajeService.enviarMensaje(mensaje).subscribe(
      res => {
        console.log(res);
        /*this.contratosService.vaciarCarrito(this.user.id_usuario).subscribe(
          res=>{
            console.log("carrito vacio")
          }
        );*/
      }
    );
  }
  logout() {
    this.authService.logout();
    this.user = null;
  }
}
