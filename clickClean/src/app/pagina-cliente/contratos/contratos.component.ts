import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { GlobalConstants } from 'src/app/models/constantes';
import { ServiciosService } from '../../services/servicios.service';
import { HabilidadesService } from '../../services/habilidades.service';
import { InmueblesService } from '../../services/inmuebles.service';
import { ContratosService } from '../../services/contratos.service';
import { AuthService } from '../../services/auth.service';
import { ScriptsloadService } from '../../services/scriptsload.service';
import { OpenPayService } from '../../services/openPay.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { chargeOP } from 'src/app/models/chargeOP';
import { CustomerOP } from 'src/app/models/customerOP';
import { CardOP } from 'src/app/models/cardsOP';
import { MensajeService } from '../../services/mensaje.service';
@Component({
  selector: 'app-contratos',
  templateUrl: './contratos.component.html',
  styleUrls: ['./contratos.component.css']
})
export class ContratosComponent implements OnInit {

  constructor(private serviciosService: ServiciosService,
    private habilidadesService: HabilidadesService,
    private contratosService: ContratosService,
    private inmuebleService: InmueblesService,
    private openPayService: OpenPayService,
    private authService: AuthService,
    private mensajeService: MensajeService,
    private scriptsLoad: ScriptsloadService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private router: Router,) { }

    carrito = 0;
  servidor: string = GlobalConstants.nodeURL;
  user: any = null;
  contratos: any = null;
  idContratoSeleccionado: number = 0;
  detalleContrato: any = null;
  showModalContrato: boolean = null;
  ngOnInit() {
    this.verificarSesion();
  }
  private async verificarSesion() {
    console.log('Verificando', 'Tipo de usuario');
    var token = this.authService.getToken();

    if (this.authService.getToken() != null) {
      this.authService.getTipoUsuario().subscribe(res => {
        let x: any = res;

        if (x == "EXPIRO") {
          this.toastr.info("Su sesion expiro");
          this.router.navigateByUrl('/auth/login');
        }
        if (token != null) {
          if (res[0].rol == "ADMINISTRADOR" || res[0].rol == "EMPLEADO" || res[0].rol == "CLIENTE" && token != null) {
            this.user = res[0];
            console.log(this.user);
            this.getContratos();
            this.getCarrito();
          }
        }
      }, err => {
        console.log(err);
      });
    } else {
      this.toastr.info("Inicie sesión")
      this.router.navigateByUrl('/auth/login');
    }
  }
  getContratos() {
    this.contratosService.obtenerMisContratos(this.user.id_usuario).subscribe(
      res => {
        console.log(res);
        this.contratos = res;
      }
    );
  }
  abrirModalContrato(id) {
    this.idContratoSeleccionado = id;
    this.contratosService.detalleContrato(id).subscribe(
      res => {
        console.log(res);
        this.detalleContrato = res;
        this.showModalContrato = true;
      }
    );

  }
  cerrarModalContrato() {
    this.showModalContrato = false;

  }
  getCarrito() {
    this.contratosService.consultarMiCarrito(this.user.id_usuario).subscribe(
      res => {
        this.carrito = res[0].length+ res[1].length;
        
      }
    );
  }
  logout() {
    this.authService.logout();
    this.user = null;
  }

}
