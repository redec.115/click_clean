import { Component, Inject, OnInit } from '@angular/core';
import { GlobalConstants } from 'src/app/models/constantes';
import { ServiciosService } from '../../services/servicios.service';
import { HabilidadesService } from '../../services/habilidades.service';
import { ContratosService } from '../../services/contratos.service';
import { AuthService } from '../../services/auth.service';
import { ClientesService } from '../../services/clientes.service';
import { InmueblesService } from '../../services/inmuebles.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ImagenesService } from '../../services/imagenes.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  constructor(private serviciosService: ServiciosService,
    private habilidadesService: HabilidadesService,
    private contratosService: ContratosService,
    private inmueblesService: InmueblesService,
    private clientesService: ClientesService,
    private imagenesService: ImagenesService,
    private authService: AuthService,
    private toastr: ToastrService,
    private router: Router) { }

  servidor: string = GlobalConstants.nodeURL;
  user: any = null;
  carrito: number = 0;
  contratos: any=null;
  detalleContrato: any=null;
  showModalContrato=false;
  inmuebles:any=null;
  imgURL1=null;
  imageName: any = "Seleccione una imagen";
  fileToUpload: File = null;
  imagePath1: any;
  ngOnInit() {
    this.verificarSesion();
  }
  private async verificarSesion() {
    console.log('Verificando', 'Tipo de usuario');
    var token = this.authService.getToken();

    if (this.authService.getToken() != null){
      this.authService.getTipoUsuario().subscribe(res => {
        let x: any = res;
        console.log(res);
        if (x == "EXPIRO") {
          this.toastr.info("Su sesion expiro");
          this.router.navigateByUrl('/auth/login');
        }
        if (token != null) {
          if (res[0].rol == "ADMINISTRADOR" || res[0].rol == "EMPLEADO" || res[0].rol == "CLIENTE" && token != null) {
            this.user = res[0];
            //console.log(res.dataUser.accessToken);
            this.obtenerMiscontratos();
            this.getCarrito();
            this.obtenerMisDirecciones();
          }
        }
      }, err => {
        console.log(err);
      });
    }else{
      this.toastr.info("Inicie sesión")
      this.router.navigateByUrl('/auth/login');
    }
  }
  obtenerMiscontratos(){
    this.contratosService.obtenerMisContratos(this.user.id_usuario).subscribe(
      res=>{
        console.log(res);
        this.contratos=res;
      }
    );
  }
  obtenerMisDirecciones(){
    this.inmueblesService.inmueblesDeCliente(this.user.id_usuario).subscribe(
      res=>{
        console.log(res);
        this.inmuebles=res;
      }
    );
  }
  handleFileInput(files: FileList) {

    this.imageName = files.item(0).name;
    //this.fileToUpload = files.item(0);
    this.fileToUpload = new File([files.item(0)], "emp_" + this.user.id_usuario + "." + files.item(0).type.substring(6));
    console.log(this.fileToUpload);
    this.guardarTemporal(files);
  }
  guardarTemporal(files) {
    console.log(files);
    if (files.length === 0)
      return;

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {

      return;
    }

    var reader = new FileReader();
    this.imagePath1 = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {

      this.imgURL1 = reader.result;
    }
    this.guardarImagen();
  }
  guardarImagen(){
    const fd = new FormData();
    fd.append('file', this.fileToUpload, this.fileToUpload.name);
    this.imagenesService.guardarImagen(fd).subscribe(
      res=>{}
    );
    let newImage = {
      id_usuario: this.user.id_usuario,
      imagen: this.fileToUpload.name
    };
    this.clientesService.agregarImagenUsuario(newImage).subscribe(
      res=>{
        this.toastr.success("Imagen actualizada");
      }
    );
  }
  getCarrito() {
    this.contratosService.consultarMiCarrito(this.user.id_usuario).subscribe(
      res => {
        this.carrito = res[0].length+ res[1].length;
        
      }
    );
  }
  abrirModalContrato(id){
    this.contratosService.detalleContrato(id).subscribe(
      res=>{
        console.log(res);
        this.detalleContrato=res;
        this.showModalContrato=true;
      }
    );
  }
  cerrarModalContrato()
  {
    this.showModalContrato=false;
  }
  logout() {
    this.authService.logout();
    this.user = null;
  }
}
