export interface chargeOP {
    source_id:              string,
    method:                 string,
    amount:                 number,
    description:            string,
    device_session_id:      string,
    customer_id:            string
}
/*
"source_id" : 'knh1omrgw6m0ujicn1rq', //id tarjeta
"method" : "card",
"amount" : 50,
"description" : "Test existing card charge",
"device_session_id": "177.228.187.107"*/