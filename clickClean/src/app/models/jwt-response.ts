export interface JwtResponse {
    dataUser:{
        id_usuario: number,
        nombre_usuario: string,
        ap_paterno_usuario: string,
        ap_materno_usuario: string,
        nombre_completo: string,
        password:string,
        email_usuario: string,
        celular_usuario: string,
        //imagen: string,
        accessToken: string,
        expiresIn: string,
        rol: string
    }
}
