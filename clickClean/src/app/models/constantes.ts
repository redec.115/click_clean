export class GlobalConstants {
    //servidor nodejs
    public static nodeURL: string = "http://localhost:3000";//"http://192.168.0.4:3000"
    public static comisionPorcentajeOpenPay: number = 0.029; //2.9%
    public static comisionFijaOpenPay: number = 2.5; //$2.5
    public static iva: number = 1.16;
    public static OpenPayID: string="moanxvkbczhg4yoifjqe";
    public static OpenPayKey: string="sk_7ee03458d51a46a38d37465116470214";
    //ng serve --open --host 0.0.0.0 --disable-host-check
    public static whatsapp: string="https://api.whatsapp.com/send?phone=528717818282&text=¿Que%roductos%y%servicios%venden?";
}