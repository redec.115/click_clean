
export interface CardOP {
    card_number:           string,
    holder_name:           string,
    expiration_year:       number,
    expiration_month:      number,
    cvv2:                  number,
    customer_id?:           string
}
/*var testCard ={
    "card_number":"4111111111111111",
    "holder_name":"Juan Perez",
    "expiration_year":"20",
    "expiration_month":"12",
    "cvv2":"111"
  };*/