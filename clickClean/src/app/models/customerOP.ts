export interface CustomerOP {
        name: string,
        last_name: string,
        email: string,
        requires_account: boolean
    
}
/*var testCreateCustomer = {
    "name":"Juan",
    "last_name":"Gonzalez",
    "email":"juan@nonexistantdomain.com",
    "requires_account":false
  };*/