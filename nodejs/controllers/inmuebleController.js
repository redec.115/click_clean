const express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
const Inmueble = require("../scripts/inmuebles");
const bcrypt = require("bcryptjs");
const jwt = require('../services/jwt');

router.get('/', function (req, res) {
    Inmueble.obtenerInmuebles( function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

router.get('/cliente/:id', function (req, res) {
    Inmueble.inmueblesDeCliente(req.params.id, function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

router.post('/', function (req, res) {
    Inmueble.crearInmueble(req.body, function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

module.exports = router;