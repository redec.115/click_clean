const express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
var Servicio = require("../scripts/servicios");
const bcrypt = require("bcryptjs");
const jwt = require('../services/jwt');

router.get('/', function (req, res) {
    Servicio.obtenerServicios(function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

router.get('/productos', function (req, res) {
    Servicio.obtenerProductos(function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

router.post('/', function (req, res) {
    Servicio.crearServicio(req.body,function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

router.post('/productos', function (req, res) {
    Servicio.agregarProducto(req.body,function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

router.post('/habilidad', function (req, res) {
    Servicio.agregarHabilidadServicio(req.body,function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});
router.post('/eliminarhabilidad', function (req, res) {
    Servicio.eliminarHabilidadServicio(req.body,function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});
router.post('/imagen', function (req, res) {
    Servicio.agregarImagenServicio(req.body,function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

router.get('/habilidades/:id', function (req, res) {
    Servicio.habilidadesDeServicio(req.params.id,function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});
router.get('/deshabilitar/:id', function (req, res) {
    Servicio.deshabilitarServicio(req.params.id,function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});
router.post('/editar', function (req, res) {
    Servicio.editarServicio(req.body,function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});
router.post('/editar', function (req, res) {
    Servicio.agregarNuevoServicio(req.body,function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});
module.exports = router;