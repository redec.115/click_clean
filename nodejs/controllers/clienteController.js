const express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
const Cliente = require("../scripts/clientes");
const bcrypt = require("bcryptjs");
const jwt = require('../services/jwt');

router.get('/', function (req, res) {
    Cliente.obtenerClientes( function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

router.get('/referencias/:id', function (req, res) {
    Cliente.referenciasCliente(req.params.id, function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

router.get('/mireferencia/:id', function (req, res) {
    Cliente.getEstoyReferenciado(req.params.id, function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});
router.post('/referencias', function (req, res) {
    Cliente.agregarReferenciaCliente(req.body, function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

router.post('/imagen', function (req, res) {
    console.log(req.body);
    Cliente.agregarImagenUsuario(req.body, function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});
module.exports = router;