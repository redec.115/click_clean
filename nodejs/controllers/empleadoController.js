const express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
var Empleado = require("../scripts/empleados");
const bcrypt = require("bcryptjs");
const jwt = require('../services/jwt');

router.get('/', function (req, res) {
    Empleado.obtenerEmpleados( function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

router.get('/contratos/:id', function (req, res) {
    Empleado.contratosDeEmpleado(req.params.id, function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});


module.exports = router;