const express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
const Habilidad = require("../scripts/habilidades");
const bcrypt = require("bcryptjs");
const jwt = require('../services/jwt');

router.get('/', function (req, res) {
    Habilidad.obtenerHabilidades( function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

router.post('/', function (req, res) {
    Habilidad.crearHabilidad(req.body, function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});
router.post('/editar', function (req, res) {
    Habilidad.editarHabilidad(req.body, function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

router.get('/empleado/:id', function (req, res) {
    Habilidad.habilidadDeEmpleado(req.params.id, function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});
router.post('/empleado/habilidad', function (req, res) {
    Habilidad.agregarHabilidadEmpleado(req.body, function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});
router.post('/empleado/eliminar', function (req, res) {
    Habilidad.eliminarHabilidadEmpleado(req.body, function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});
module.exports = router;