var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
var Busboy = require('busboy');
var path = require('path');
var Auth = require('../scripts/auth');
var fs = require('fs');
const jwt = require('../services/jwt');

router.post('/', function (req, res) {
  if (req.headers.token != null) {
    let dec = jwt.decode(req.headers.token);
    if (Date.now() <= (dec.exp * 1000)) {
      if (dec.rol == "ADMINISTRADOR" || dec.rol == "EMPLEADO" || dec.rol == "CLIENTE") {
        var busboy = new Busboy({ headers: req.headers });
        busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
          console.log('File [' + fieldname + ']: filename: ' + filename + ', encoding: ' + encoding + ', mimetype: ' + mimetype);
          var saveTo = path.join(__dirname, '../public/images/' + filename);

          file.on('finish', function () {
            res.writeHead(200, { 'Connection': 'close' });
            res.end("");
          });
          file.pipe(fs.createWriteStream(saveTo));
        });
        busboy.on('finish', function () {
          finished = true;
        });
        busboy.on('error', function () {
          console.log('Busboy error');
          res.send('Finished');
        });
        return req.pipe(busboy);

        //*/
      } else {
        res.json("No tiene permisos para ver esto");
      }
    } else {
      res.json("EXPIRO");
    }
  } else {
    res.json("No tiene permisos para ver esto");
  }
});

router.post('/actualizar', function (req, res) {
  if (req.headers.token != null) {
    let dec = jwt.decode(req.headers.token);
    if (Date.now() <= (dec.exp * 1000)) {
      if (dec.tipo_usuario == "ADMINISTRADOR" || dec.tipo_usuario == "EMPLEADO" || dec.tipo_usuario == "CLIENTE") {
        /*Auth.insertarImagen(req.body, function (err, rows) {
          if (err) {
            res.status(400).json(err);
          }
          else {
            res.json('Nueva imagen cambiada');
          }
        });*/
      } else {
        res.json("No tiene permisos para ver esto");
      }
    } else {
      res.json("EXPIRO");
    }
  } else {
    res.json("No tiene permisos para ver esto");
  }

});
router.get('/:imageFile', function (req, res) {
  
        let imageFile = req.params.imageFile;
        let filePath = './public/images/' + imageFile;
        fs.exists(filePath, (exists) => {
          (exists) ? res.sendFile(path.resolve(filePath)) : res.status(404).send({ message: 'The image doesn´t exist' });
        });

      
});
router.delete('/:imageFile', function (req, res) {
  if (req.headers.token != null) {
    let dec = jwt.decode(req.headers.token);
    if (Date.now() <= (dec.exp * 1000)) {
      if (dec.rol == "ADMINISTRADOR" || dec.rol == "EMPLEADO" || dec.rol == "CLIENTE") {
        let imageFile = req.params.imageFile;
        let filePath = './public/images/' + imageFile;
        fs.exists(filePath, (exists) => {
          (exists) ? fs.unlink(filePath) : res.status(404).send({ message: 'The image doesn´t exist' });
        });
      } else {
        res.json("No tiene permisos para ver esto");
      }
    } else {
      res.json("EXPIRO");
    }
  } else {
    res.json("No tiene permisos para ver esto");
  }
});
module.exports = router;