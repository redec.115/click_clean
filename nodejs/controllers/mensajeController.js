const express = require('express');
var bodyParser = require('body-parser');
const configMensaje = require('./configMensaje');
var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
const jwt = require('../services/jwt');


router.post('/enviarMensaje', function (req, res) {
   
             configMensaje.sendMail(req.body,function (err, result) {
                 console.log(result);
                    if(!err){
                        res.json(result);
                    }else{
                        res.json(err);
                    }
                });
                

});

module.exports = router;

