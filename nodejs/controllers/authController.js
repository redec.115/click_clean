var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
const Auth = require('../scripts/auth');
var bcrypt = require('bcryptjs');
let jwt = require('../services/jwt');
var path = require('path');


router.post('/email', function (req, res) {
    Auth.buscarUsuarioPorCorreo(req.body.email, function (err, result) {
        if(!err){
            console.log(result[0][0].id_usuario);
            res.json(result[0][0]);
        }else{
            res.json(err);
        }
    });
});

router.post('/login', function (req, res) {
    let params = req.body;
    let email = params.email;
    let password = params.password;
    let social = params.social;
    Auth.buscarUsuarioPorCorreo(email, function (err, result) {
        if (err) {
            res.status(400).json(err);
        }
        else if (result[0][0] != null) {
            //res.json('Se encontro el usuario');
            console.log(result[0]);
            if (params.social != null) {
                bcrypt.compare(social, result[0][0].social, (err, check) => {
                    if (check == true) {
                        
                        const dataUser = {
                            id_usuario: result[0][0].id_usuario,
                            nombre_usuario: result[0][0].nombre_usuario,
                            email_usuario: result[0][0].email_usuario,
                            celular_usuario: result[0][0].celular_usuario,
                            //imagen: result[0].imagen,
                            accessToken: jwt.createToken(result[0][0]),
                            expiresIn: 24 * 60 * 60,//un dia 60 seg * 60 min * 24hrs
                            rol: result[0][0].rol,
                            nombre_completo:result[0][0].nombre_usuario+" "+result[0][0].apellido_paterno_usuario+" "+result[0][0].apellido_materno_usuario 
                        }

                        res.send({ dataUser });
                        //res.status(200).send({token:jwt.createToken(result[0])});
                        //req.session.loggedin = true;
                        //req.session.username = username;
                        //res.redirect('/home');
                    } else {
                        res.json("Contraseña facebook");
                    }
                });
            } else
                bcrypt.compare(password, result[0][0].password, (err, check) => {
                    if (check == true) {
                        const dataUser = {
                            id_usuario: result[0][0].id_usuario,
                            nombre_usuario: result[0][0].nombre_usuario,
                            email_usuario: result[0][0].email_usuario,
                            celular_usuario: result[0][0].celular_usuario,
                            //imagen: result[0].imagen,
                            accessToken: jwt.createToken(result[0][0]),
                            expiresIn: 24 * 60 * 60,//un dia 60 seg * 60 min * 24hrs
                            rol: result[0][0].rol,
                            nombre_completo:result[0][0].nombre_usuario+" "+result[0][0].ap_paterno_usuario+" "+result[0][0].ap_materno_usuario 
                        }

                        res.send({ dataUser });
                        //res.status(200).send({token:jwt.createToken(result[0])});
                        //req.session.loggedin = true;
                        //req.session.username = username;
                        //res.redirect('/home');
                    } else {
                        res.json("Contraseña Incorrecta");
                    }
                });
        } else {
            res.json("No existe el usuario");
        }
    });
});
//register
router.post('/register', function (req, res) {
    let params = req.body;
    
        bcrypt.hash(params.password, 10, (err, hash) => {
            if (err) {
                res.status(404).json(err);
            }
            else {
                req.password = hash;
                Auth.crearUsuario(req.body, req.password, function (err, count) {

                    if (err) {
                        res.status(400).json(err);
                    }
                    else {
                        res.json(count[0]);
                    }
                });
            }
        });
});

router.get('/tipo', function (req, res) {
    if (req.headers.token != null) {
        let dec = jwt.decode(req.headers.token);
        if (Date.now() <= (dec.exp * 1000)) {
            if (dec.rol == "ADMINISTRADOR" || dec.rol == "CLIENTE" || dec.rol == "EMPLEADO") {
                Auth.consultartipousuario(req.headers.email, function (err, count) {
                    if (err) {
                        console.log("yes");
                        res.status(400).json(err);
                    }
                    else {
                        res.json(count[0]);
                    }
                });
            } else {
                res.json("No tiene permisos");
            }
        } else {
            res.json("EXPIRO");
        }
    } else {
        res.json("No tiene permisos para ver esto");
    }
});

router.get('/usuario/:id', function (req, res) {
    Auth.datosDeUsuario(req.params.id, function (err, result) {
        if(!err){
            res.json(result[0][0]);
        }else{
            res.json(err);
        }
    });
});

router.get('/calificacion/:id', function (req, res) {
    Auth.calificacionUsuario(req.params.id, function (err, result) {
        if(!err){
            res.json(result[0][0]);
        }else{
            res.json(err);
        }
    });
});

module.exports = router;