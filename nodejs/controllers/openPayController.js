const express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
const ScriptOpenPay = require("../scripts/openpay");
const bcrypt = require("bcryptjs");
const jwt = require('../services/jwt');
var assert = require('assert');
var _ = require('underscore');
var request = require('request');

var Openpay = require('../lib/openpay');
/*Sandbox*/
//sk_ed05f1de65fa4a67a3d3056a4efa2905
//m1qp3av1ymcfufkuuoah
var openpay = new Openpay('moanxvkbczhg4yoifjqe', 'sk_7ee03458d51a46a38d37465116470214');
openpay.setTimeout(10000);

var enableLogging = true;
var testCreateCharges = true;
var testCreatePayouts = false;
var testBankAccountId = 'bmopptj5st1hx8ddouha';
var testCreateCustomer = {
    "name": "Juan",
    "last_name": "Gonzalez",
    "email": "juan@nonexistantdomain.com",
    "requires_account": false
};
var testTransfer = {
    "customer_id": "aeflth2btknt300jqjjn",
    "amount": 1.50,
    "description": "Test transfer"
};
var testCard = {
    "card_number": "411111111111111",
    "holder_name": "Juan Perez",
    "expiration_year": "20",
    "expiration_month": "12",
    "cvv2": "111"
};
var testExistingCardCharge = {
    "source_id": 'knh1omrgw6m0ujicn1rq', //id tarjeta
    "method": "card",
    "amount": 50,
    "description": "Test existing card charge",
    "device_session_id": "177.228.187.107"
};
router.get('/customer/:id', function (req, res) {//testCreateCustomer
    ScriptOpenPay.getOpenPayIDCustomer(req.params.id, function (err, result) {
        if (!err) {
            res.json(result[0]);
        } else {
            res.json(err);
        }
    });
});
router.post('/customer', function (req, res) {//testCreateCustomer
    openpay.customers.create(req.body, function (err, result) {
        if (!err) {
            req.body.id_openpay = result.id;
            ScriptOpenPay.agregarOpenPay(req.body, function (errr, resultt) {
                if (!errr) {
                    res.json(result);
                } else {
                    res.json(errr);
                }
            });
        } else {
            res.json(err);
        }
    });
});

router.delete('/customer/:id', function (req, res) {
    openpay.customers.delete(req.params.id, function (err, result) {
        if (!err) {
            res.json("Cliente eliminado");
        } else {
            res.json(err);
        }
    });
});
//lista tarjetas
router.get('/card', function (req, res) {
    openpay.cards.list({}, function (error, body, response) {
        if (!error) {
            res.json(body);
        } else {
            res.json(error);
        }
    });
});
//crear tarjetas
router.post('/card/customer', function (req, res) {//'av0pto2mmrq7cdlbvzv7' testCard
    let newCardOp = {
        card_number: req.body.card_number,
        cvv2: req.body.cvv2,
        expiration_month: req.body.expiration_month,
        expiration_year: req.body.expiration_year,
        holder_name: req.body.holder_name,
    }
    openpay.customers.cards.create(req.body.customer_id, newCardOp, function (error, body, response) {
        if (!error) {
            res.json(body);
        } else {
            res.json(error);
        }
    });
});
//get tarjetas
router.get('/card/customer/:id', function (req, res) {//'av0pto2mmrq7cdlbvzv7'
    openpay.customers.cards.list(req.params.id, {}, function (error, body, response) {
        if (!error) {
            res.json(body);
        } else {
            res.json(error);
        }
    });

});

router.post('/card/customer/delete', function (req, res) {//'av0pto2mmrq7cdlbvzv7'
    openpay.customers.cards.delete(req.body.customer_id,req.body.card_id, function (error, body, response) {
        if (!error) {
            res.json(body);
        } else {
            res.json(error);
        }
    });
});

router.post('/charge', function (req, res) {//'av0pto2mmrq7cdlbvzv7' testExistingCardCharge
    let cargo = {
        amount: req.body.amount,
        description: req.body.description,
        device_session_id: req.body.device_session_id,
        source_id: req.body.source_id,
        method: req.body.method
    }

    openpay.customers.charges.create(req.body.customer_id, cargo, function (error, body, response) {

        if (!error) {
            res.json(body);
        } else {
            res.json(error);
        }
    });
});
module.exports = router;