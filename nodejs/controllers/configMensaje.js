const nodemailer = require('nodemailer');
var exphbs = require('express-handlebars');
const hbs = require('nodemailer-express-handlebars');

async function sendMail(formulario,callback) {

  var transporter = nodemailer.createTransport({
    //host:
    service: 'gmail',
    secure: true,
    secureConnection: true,
    //port
    //requireTLS:
    auth: {
      user: 'p.ibarrag14@gmail.com', // Cambialo por tu email
      pass: 'yywycqobbacxznsq' // Cambialo por tu password
    }
  });
  console.log(formulario);
  var i = 0;
  var imagenes = [];
  if (formulario.servicio != null) {
    while (i < formulario.servicio.length) {
      if (formulario.servicio[i].imagen_servicio != null) {
        imagenes[i] = {
          filename: formulario.servicio[i].imagen_servicio,//nombre del archivo
          path: 'public/images/' + formulario.servicio[i].imagen_servicio,//direccion completa
          cid: formulario.servicio[i].imagen_servicio//identificador de la imagen
        }
      }
      else {
        imagenes[i] = {
          filename: "mascota.jpg",
          path: 'public/images/mascota.jpg',
          cid: 'logo'
        }
      }
      i++;
    }
    console.log(imagenes);
  }
  //Correo que reciben los admin al tener una compra
  if (formulario.para == "nuevocontrato") {
    const handlebarOptions = {
      viewEngine: {
        extName: '.hbs',
        partialsDir: './views',
        layoutsDir: './views',
        defaultLayout: 'nuevocontrato.hbs',

      },
      viewPath: './views',
      extName: '.hbs'
    };

    var data = "datos";

    transporter.set()
    transporter.use('compile', hbs(handlebarOptions));


    const mailOptions = {
      from: `”${formulario.nombre}” <${formulario.email}>`,
      to: formulario.destinatario, // Cambia esta parte por el destinatario
      subject: formulario.asunto,


      template: 'nuevocontrato',
      context: {
        datos: formulario
      },
      attachments: imagenes
    };

    console.log('Enviando mensaje a socio');
    i = 0;
    return transporter.sendMail(mailOptions, callback);
  }
  else if (formulario.para == "prueba") {
    const handlebarOptions = {
      viewEngine: {
        extName: '.hbs',
        partialsDir: './views',
        layoutsDir: './views',
        defaultLayout: 'prueba.hbs',

      },
      viewPath: './views',
      extName: '.hbs'
    };

    var data = "datos";

    transporter.set()
    transporter.use('compile', hbs(handlebarOptions));


    const mailOptions = {
      from: `”${formulario.nombre}” <${formulario.email}>`,
      to: formulario.destinatario, // Cambia esta parte por el destinatario
      subject: formulario.asunto,


      template: 'prueba',
      context: {
        datos: formulario
      },
      attachments: imagenes
    };

    console.log('Enviando mensaje prueba');
    i = 0;
    return transporter.sendMail(mailOptions, callback);
    
    
  }
  //Correo que 

}
module.exports ={
  sendMail
}