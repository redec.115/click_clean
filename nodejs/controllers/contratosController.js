const express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
const Contrato = require("../scripts/contratos");
const bcrypt = require("bcryptjs");
const jwt = require('../services/jwt');

router.get('/miscontratos/:id', function (req, res) {
    Contrato.obtenerMisContratos(req.params.id, function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

router.get('/miscompras/:id', function (req, res) {
    Contrato.obtenerMisCompras(req.params.id, function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

router.get('/detalle/:id', function (req, res) {
    Contrato.detalleContrato(req.params.id, function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

router.get('/detallecompra/:id', function (req, res) {
    Contrato.detalleCompra(req.params.id, function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

router.post('/carrito/servicio', function (req, res) {
    Contrato.agregarServicioCarrito(req.body, function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});
router.post('/carrito/producto', function (req, res) {
    Contrato.agregarProductoCarrito(req.body, function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});
router.post('/carrito/servicio/eliminar', function (req, res) {
    Contrato.eliminarServicioCarrito(req.body, function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

router.post('/carrito/producto/eliminar', function (req, res) {
    Contrato.eliminarProductoCarrito(req.body, function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

router.delete('/carrito/servicio/:id', function (req, res) {
    Contrato.vaciarCarrito(req.params.id, function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

router.get('/carrito/:id', function (req, res) {
    Contrato.consultarMiCarrito(req.params.id, function (err, result) {
        if(!err){
            res.json(result);
        }else{
            res.json(err);
        }
    });
});

router.get('/servicios/empleados/:id', function (req, res) {
    Contrato.getEmpleadosDeServicio(req.params.id, function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

router.post('/carrito/contratar', function (req, res) {
    Contrato.agregarContrato(req.body, function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

router.post('/carrito/contrata/servicios', function (req, res) {
    Contrato.agregarServicioContrato(req.body, function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

router.get('/estatus/activos', function (req, res) {
    Contrato.getContratosActivos(function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

router.get('/estatus/proximos', function (req, res) {
    Contrato.getContratosProximos(function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});

router.get('/estatus/historial', function (req, res) {
    Contrato.getContratosHistorial(function (err, result) {
        if(!err){
            res.json(result[0]);
        }else{
            res.json(err);
        }
    });
});
module.exports = router;