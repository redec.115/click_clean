const app = require("./app");
const port = process.env.PORT || 3000;
const hostname = "192.168.0.4";
const server = app.listen(port, function () {
    console.log("Servidor en puerto: " + port);
    const all_routes = require("express-list-endpoints");
    console.log(all_routes(app));
});