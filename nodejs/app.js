const express = require('express');
var app = express();
const cors = require('cors');

app.use(cors());
var ClienteController = require("./controllers/clienteController");
app.use('/clientes', ClienteController);

var ContratosController = require("./controllers/contratosController");
app.use('/contratos', ContratosController);

var EmpleadosController = require("./controllers/empleadoController");
app.use('/empleados', EmpleadosController);

var HabilidadesController = require("./controllers/habilidadesController");
app.use('/habilidades', HabilidadesController);

var InmueblesController = require("./controllers/inmuebleController");
app.use('/inmuebles', InmueblesController);


var ServiciosController = require("./controllers/serviciosController");
app.use('/servicios', ServiciosController);

var AuthController = require("./controllers/authController");
app.use('/auth', AuthController);

var ImageController = require("./controllers/imageController");
app.use('/imagenes', ImageController);

var OpenPayController = require("./controllers/openPayController");
app.use('/openpay', OpenPayController);

var MensajeController = require("./controllers/mensajeController");
app.use('/mensaje', MensajeController);
module.exports = app;

