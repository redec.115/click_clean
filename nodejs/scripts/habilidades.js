var db = require('../db');
var Habilidad = {
    
    obtenerHabilidades: function(callback)
    {
        return db.query('CALL getHabilidades();',callback);
    },
    habilidadDeEmpleado: function(id,callback)
    {
        return db.query('CALL get_Habilidades_Empleado(?);',[id],callback);
    },
    crearHabilidad: function(datos,callback)
    {
        return db.query('CALL crearHabilidad(?,?,?);',[
            datos.nombre,
            datos.descripcion,
            datos.precio],callback);
    },
    editarHabilidad: function(datos,callback)
    {
        return db.query('CALL editar_habilidad(?,?,?,?);',[
            datos.id_habilidad,
            datos.nombre_habilidad,
            datos.descripcion_habilidad,
            datos.precio_habilidad],callback);
    },
    agregarHabilidadEmpleado: function (datos, callback) {
        return db.query('CALL agregarHabilidadEmpleado(?,?);', [
            datos.id_empleado,
            datos.id_habilidad], callback);
    },
    eliminarHabilidadEmpleado: function (datos, callback) {
        return db.query('CALL eliminarHabilidadEmpleado(?,?);', [datos.id_habilidad,
            datos.id_empleado,
            ], callback);
    },
}
module.exports = Habilidad;