var db = require('../db');
var OpenPay = {

    agregarOpenPay: function (data,callback) {
        return db.query('CALL agregarOpenPay(?,?);',[data.id_openpay,data.id_usuario], callback);
    },
    getOpenPayIDCustomer: function (id,callback) {
        return db.query('CALL getOpenPayID(?);',[id], callback);
    },
    
}
module.exports = OpenPay;