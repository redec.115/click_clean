var db = require('../db');
var Auth = {
    
    buscarUsuarioPorCorreo: function(email,callback)
    {
        return db.query('CALL buscarUsuarioPorCorreo(?);',[email],callback);
    },
    crearUsuario: function(datos,password,callback)
    {
        return db.query('CALL crearUsuario(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);',[
            datos.nombre_usuario,
            datos.ap_paterno_usuario,
            datos.ap_materno_usuario,
            datos.rfc_usuario,
            datos.curp_usuario,
            datos.fecha_nacimiento_usuario,
            datos.telcasa_usuario,
            datos.celular_usuario,
            datos.email_usuario,
            datos.calle_usuario,
            datos.numero_casa_usuario,
            datos.colonia_usuario,
            datos.ciudad_usuario,
            datos.estado_usuario,
            password,
            datos.id_rol],callback);
    },
    datosDeUsuario: function (id, callback) {
        return db.query('CALL get_Datos_Cliente(?);', [id], callback);
    },
    calificacionUsuario: function (id, callback) {
        return db.query('CALL getCalificacionTotal(?);', [id], callback);
    },
    consultartipousuario: function (email, callback) {
        return db.query('CALL getTipoUsuario(?);', [email], callback);
    },
}
module.exports = Auth;