var db = require('../db');
var Cliente = {

    obtenerClientes: function (callback) {
        return db.query('CALL getClientes();', callback);
    },
    
    referenciasCliente: function (id, callback) {
        return db.query('CALL getReferenciasCliente(?);', [id], callback);
    },
    agregarReferenciaCliente: function (data, callback) {
        return db.query('CALL agregarCodigoReferencia(?,?);', [data.codigo,data.id_cliente], callback);
    },
    getEstoyReferenciado: function (id_cliente, callback) {
        return db.query('CALL getEstoyReferenciado(?);', [id_cliente], callback);
    },
    agregarImagenUsuario: function (data, callback) {
        return db.query('CALL agregarImagenUsuario(?,?);', [data.id_usuario,data.imagen], callback);
    },
}
module.exports = Cliente;