var db = require('../db');
var Inmueble = {

    obtenerInmuebles: function (callback) {
        return db.query('CALL getInmuebles();', callback);
    },
    inmueblesDeCliente: function (id, callback) {
        return db.query('CALL get_Inmuebles_Cliente(?);', [id], callback);
    },
    crearInmueble: function (datos, callback) {
        return db.query('CALL crearInmueble(?,?,?,?,?,?,?,?,?);', [
            datos.tipo,
            datos.tamano,
            datos.numero_plantas,
            datos.calle,
            datos.numero,
            datos.colonia,
            datos.ciudad,
            datos.estado,
            datos.id_cliente], callback);
    },
}
module.exports = Inmueble;