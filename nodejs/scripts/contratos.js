var db = require('../db');
var Contrato = {

    obtenerMisContratos: function (id,callback) {
        return db.query('CALL getMisContratos(?);',id, callback);
    },
    obtenerMisCompras: function (id,callback) {
        return db.query('CALL getMisCompras(?);',id, callback);
    },
    detalleContrato: function (id, callback) {
        return db.query('CALL getDetalleContrato(?);', [id], callback);
    },
    detalleCompra: function (id, callback) {
        return db.query('CALL getDetalleCompra(?);', [id], callback);
    },
    agregarServicioCarrito: function (datos, callback) {
        return db.query('CALL agregarServicioCarrito(?,?);', [datos.id_cliente,datos.id_servicio], callback);
    },
    agregarProductoCarrito: function (datos, callback) {
        return db.query('CALL agregarProductoCarrito(?,?,?);', [datos.id_cliente,datos.id_producto,datos.cantidad], callback);
    },
    eliminarServicioCarrito: function (datos, callback) {
        return db.query('CALL eliminarServicioCarrito(?,?);', [datos.id_cliente,datos.id_servicio], callback);
    },
    eliminarProductoCarrito: function (datos, callback) {
        return db.query('CALL eliminarProductoCarrito(?,?);', [datos.id_cliente,datos.id_producto], callback);
    },
    vaciarCarrito: function (id, callback) {
        return db.query('CALL vaciarCarrito(?);', [id], callback);
    },
    consultarMiCarrito: function (id, callback) {
        return db.query('CALL consultarMiCarrito(?);', [id], callback);
    },
    getEmpleadosDeServicio: function (id, callback) {
        return db.query('CALL getEmpleadosDeServicio(?);', [id], callback);
    },
    agregarContrato: function (datos, callback) {
        return db.query('CALL agregarContrato(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);', [datos.id_cliente,datos.id_inmueble,
            datos.id_empleado,datos.inicio_contrato,datos.fin_contrato,datos.lunes,datos.martes,datos.miercoles,
            datos.jueves,datos.viernes,datos.sabado,datos.domingo,datos.hora_inicio,datos.hora_fin,datos.total_contrato], callback);
    },
    agregarServicioContrato: function (data, callback) {
        return db.query('CALL agregarServicioContrato(?,?,?);', [
            data.id_servicio,  
            data.id_contrato,  
            data.total_servicio_contrato], callback);
    },
    getContratosActivos: function ( callback) {
        return db.query('CALL get_Contratos_Activos();', callback);
    },
    getContratosProximos: function ( callback) {
        return db.query('CALL get_contratos_proximos();', callback);
    },
    getContratosHistorial: function ( callback) {
        return db.query('CALL get_contratos_historial();', callback);
    },
}
module.exports = Contrato;