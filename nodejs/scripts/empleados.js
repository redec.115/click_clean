var db = require('../db');
var Empleado = {

    obtenerEmpleados: function (callback) {
        return db.query('CALL getEmpleados();', callback);
    },
    contratosDeEmpleado: function (id,callback) {
        return db.query('CALL get_contratosDeEmpleado(?);',id, callback);
    },
}
module.exports = Empleado;