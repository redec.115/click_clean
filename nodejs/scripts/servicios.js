var db = require('../db');
var Servicio = {

    obtenerServicios: function (callback) {
        return db.query('CALL getServicios();', callback);
    },
    obtenerProductos: function (callback) {
        return db.query('CALL getProductos();', callback);
    },
    crearServicio: function (datos, callback) {
        return db.query('CALL crearServicio(?,?,?);', [
            datos.nombre_servicio,
            datos.caracteristicas_servicio,
            datos.imagen_servicio], callback);
    },
    agregarProducto: function (datos, callback) {
        return db.query('CALL agregarProducto(?,?,?);', [
            datos.nombre_producto,
            datos.unidades_producto,
            datos.precio_producto], callback);
    },
    agregarHabilidadServicio: function (datos, callback) {
        return db.query('CALL agregarHabilidadServicio(?,?);', [
            datos.id_servicio,
            datos.id_habilidad], callback);
    },
    eliminarHabilidadServicio: function (datos, callback) {
        return db.query('CALL eliminar_habilidad_servicio(?,?);', [
            datos.id_servicio,
            datos.id_habilidad], callback);
    },
    agregarImagenServicio: function (datos, callback) {
        return db.query('CALL agregarImagenServicio(?,?);', [
            datos.id_servicio,
            datos.imagen_servicio], callback);
    },
    habilidadesDeServicio: function(id,callback)
    {
        return db.query('CALL getHabilidadesDeServicio(?);',[id],callback);
    },
    deshabilitarServicio: function(id,callback)
    {
        return db.query('CALL deshabilitarServicio(?);',[id],callback);
    },
    editarServicio: function(datos,callback)
    {
        return db.query('CALL editarServicio(?,?,?,?);',[datos.id_servicio,datos.nombre_servicio,datos.caracteristicas_servicio,datos.imagen_servicio],callback);
    },
}
module.exports = Servicio;