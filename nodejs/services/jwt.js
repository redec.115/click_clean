let jwt = require('jwt-simple');
let moment = require('moment');
let secret = 'pablo';

function createToken(user) {
    let payload = {
        id: user.id_usuario,
        nombre: user.nombre_usuario,
        rol: user.rol,
        iat: moment().unix(),
        exp: moment().add(30, 'days').unix()
    };
    return jwt.encode(payload, secret);
}
function decode(tok) {
    try {
        let decoded = jwt.decode(tok, secret);
        return decoded;
    } catch (error) {
        return error;
    }
    
        

}

module.exports = {
    createToken, decode
}