-- MySQL Workbench Forward Engineering

-- -----------------------------------------------------
-- Schema click_clean
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema click_clean
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `click_clean` DEFAULT CHARACTER SET utf8 ;
USE `click_clean` ;

-- -----------------------------------------------------
-- Table `click_clean`.`roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `click_clean`.`roles` (
  `id_rol` INT NOT NULL AUTO_INCREMENT,
  `rol` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_rol`),
  UNIQUE INDEX `id_rol_UNIQUE` (`id_rol` ASC) VISIBLE,
  UNIQUE INDEX `rol_UNIQUE` (`rol` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `click_clean`.`usuarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `click_clean`.`usuarios` (
  `id_usuario` INT NOT NULL AUTO_INCREMENT,
  `nombre_usuario` VARCHAR(75) NOT NULL,
  `ap_paterno_usuario` VARCHAR(75) NULL,
  `ap_materno_usuario` VARCHAR(75) NULL,
  `rfc_usuario` VARCHAR(25) NULL,
  `curp_usuario` VARCHAR(25) NULL,
  `fecha_nacimiento_usuario` DATE NULL,
  `fecha_ingreso_usuario` DATE NULL,
  `telcasa_usuario` VARCHAR(15) NULL,
  `celular_usuario` VARCHAR(15) NULL,
  `email_usuario` VARCHAR(75) NULL,
  `calle_usuario` VARCHAR(75) NULL,
  `numero_casa_usuario` VARCHAR(10) NULL,
  `colonia_usuario` VARCHAR(75) NULL,
  `ciudad_usuario` VARCHAR(45) NULL,
  `estado_usuario` VARCHAR(45) NULL,
  `password` VARCHAR(45) NOT NULL,
  `id_rol` INT NOT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE INDEX `id_cliente_UNIQUE` (`id_usuario` ASC) VISIBLE,
  INDEX `fk_clientes_roles1_idx` (`id_rol` ASC) VISIBLE,
  CONSTRAINT `fk_clientes_roles1`
    FOREIGN KEY (`id_rol`)
    REFERENCES `click_clean`.`roles` (`id_rol`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = '	';


-- -----------------------------------------------------
-- Table `click_clean`.`referencias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `click_clean`.`referencias` (
  `id_referencia` INT NOT NULL AUTO_INCREMENT,
  `nombre_ref` VARCHAR(75) NOT NULL,
  `ap_paterno_ref` VARCHAR(75) NULL,
  `ap_materno_ref` VARCHAR(75) NULL,
  `telcasa_ref` VARCHAR(15) NULL,
  `celular_ref` VARCHAR(15) NULL,
  `calle_ref` VARCHAR(75) NULL,
  `numero_casa_ref` VARCHAR(10) NULL,
  `colonia_ref` VARCHAR(75) NULL,
  `ciudad_ref` VARCHAR(75) NULL,
  `id_empleado` INT NOT NULL,
  PRIMARY KEY (`id_referencia`),
  UNIQUE INDEX `id_referencia_UNIQUE` (`id_referencia` ASC) VISIBLE,
  INDEX `fk_referencias_clientes1_idx` (`id_empleado` ASC) VISIBLE,
  CONSTRAINT `fk_referencias_clientes1`
    FOREIGN KEY (`id_empleado`)
    REFERENCES `click_clean`.`usuarios` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `click_clean`.`habilidades`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `click_clean`.`habilidades` (
  `id_habilidad` INT NOT NULL AUTO_INCREMENT,
  `nombre_habilidad` VARCHAR(100) NOT NULL,
  `descripcion_habilidad` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_habilidad`),
  UNIQUE INDEX `id_habilidad_UNIQUE` (`id_habilidad` ASC) VISIBLE,
  UNIQUE INDEX `nombre_habilidad_UNIQUE` (`nombre_habilidad` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `click_clean`.`servicios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `click_clean`.`servicios` (
  `id_servicio` INT NOT NULL AUTO_INCREMENT,
  `nombre_servicio` VARCHAR(50) NOT NULL,
  `caracteristicas_servicio` VARCHAR(250) NOT NULL,
  `costo_servicio` VARCHAR(45) NULL,
  PRIMARY KEY (`id_servicio`),
  UNIQUE INDEX `id_servicio_UNIQUE` (`id_servicio` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `click_clean`.`inmuebles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `click_clean`.`inmuebles` (
  `id_inmueble` INT NOT NULL AUTO_INCREMENT,
  `tipo_inm` VARCHAR(50) NOT NULL,
  `tamano_inm` VARCHAR(45) NULL,
  `numero_plantas_inm` INT NOT NULL,
  `calle_inm` VARCHAR(75) NOT NULL,
  `numero_inm` VARCHAR(75) NOT NULL,
  `colonia_inm` VARCHAR(75) NOT NULL,
  `ciudad_inm` VARCHAR(75) NOT NULL,
  `estado_inm` VARCHAR(75) NOT NULL,
  `id_cliente` INT NOT NULL,
  PRIMARY KEY (`id_inmueble`),
  UNIQUE INDEX `id_inmueble_UNIQUE` (`id_inmueble` ASC) VISIBLE,
  INDEX `fk_inmuebles_clientes1_idx` (`id_cliente` ASC) VISIBLE,
  CONSTRAINT `fk_inmuebles_clientes1`
    FOREIGN KEY (`id_cliente`)
    REFERENCES `click_clean`.`usuarios` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `click_clean`.`contratos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `click_clean`.`contratos` (
  `id_contrato` INT NOT NULL AUTO_INCREMENT,
  `id_servicio` INT NOT NULL,
  `id_cliente` INT NOT NULL,
  `id_inmueble` INT NOT NULL,
  `id_empleado` INT NOT NULL,
  `inicio_contrato` DATETIME NOT NULL,
  `fin_contrato` DATETIME NULL,
  PRIMARY KEY (`id_contrato`),
  UNIQUE INDEX `id_contrato_UNIQUE` (`id_contrato` ASC) VISIBLE,
  INDEX `fk_contratos_servicios1_idx` (`id_servicio` ASC) VISIBLE,
  INDEX `fk_contratos_clientes1_idx` (`id_cliente` ASC) VISIBLE,
  INDEX `fk_contratos_inmuebles1_idx` (`id_inmueble` ASC) VISIBLE,
  INDEX `fk_contratos_clientes2_idx` (`id_empleado` ASC) VISIBLE,
  CONSTRAINT `fk_contratos_servicios1`
    FOREIGN KEY (`id_servicio`)
    REFERENCES `click_clean`.`servicios` (`id_servicio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contratos_clientes1`
    FOREIGN KEY (`id_cliente`)
    REFERENCES `click_clean`.`usuarios` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contratos_inmuebles1`
    FOREIGN KEY (`id_inmueble`)
    REFERENCES `click_clean`.`inmuebles` (`id_inmueble`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contratos_clientes2`
    FOREIGN KEY (`id_empleado`)
    REFERENCES `click_clean`.`usuarios` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `click_clean`.`habilidades_de_empleado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `click_clean`.`habilidades_de_empleado` (
  `id_habilidad` INT NOT NULL,
  `id_empleado` INT NOT NULL,
  PRIMARY KEY (`id_habilidad`, `id_empleado`),
  INDEX `fk_habilidades_has_empleados_habilidades1_idx` (`id_habilidad` ASC) VISIBLE,
  INDEX `fk_habilidades_de_empleado_clientes1_idx` (`id_empleado` ASC) VISIBLE,
  CONSTRAINT `fk_habilidades_has_empleados_habilidades1`
    FOREIGN KEY (`id_habilidad`)
    REFERENCES `click_clean`.`habilidades` (`id_habilidad`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_habilidades_de_empleado_clientes1`
    FOREIGN KEY (`id_empleado`)
    REFERENCES `click_clean`.`usuarios` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE `click_clean`.`calificaciones` (
  `id_calificacion` INT NOT NULL AUTO_INCREMENT,
  `calificacion` FLOAT NULL,
  `id_contrato` INT NOT NULL,
  `id_usuario` INT NOT NULL,
  PRIMARY KEY (`id_calificacion`),
  UNIQUE INDEX `id_calificacion_UNIQUE` (`id_calificacion` ASC) VISIBLE,
  INDEX `fk_cali_contrato_idx` (`id_contrato` ASC) VISIBLE,
  INDEX `fk_cali_usuario_idx` (`id_usuario` ASC) VISIBLE,
  CONSTRAINT `fk_cali_contrato`
    FOREIGN KEY (`id_contrato`)
    REFERENCES `click_clean`.`contratos` (`id_contrato`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_cali_usuario`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `click_clean`.`usuarios` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)ENGINE = InnoDB;


